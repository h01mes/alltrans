isUndef  = function(a){return typeof a == "undefined";};
isNull   = function(a){return typeof a == "object" && !a;};
var isFF=(navigator.userAgent.toLowerCase().indexOf("firefox")!=-1);
var isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var isIE6 = isIE && ([/MSIE (\d+)\.0/i.exec(navigator.userAgent)][0][1] == 6);
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
var isChrome = (navigator.userAgent.indexOf("Chrome") != -1) ? true : false;
var isSafari = (navigator.userAgent.indexOf("Safari") != -1) ? true : false;

$PU = function(parameter, url)
{
	url = isUndef(url) ? location.href : url;
	var result = url.match(new RegExp("[\#|\?]([^#]*)[\#|\?]?"));
	url = "&" + (isNull(result) ? "" : result[1]);
	result = url.match(new RegExp("&"+parameter+"=", "i"));
	return isNull(result) ? undefined : url.substr(result.index+1).split("&")[0].split("=")[1];
};

var C_NICKNAME = filter(decodeURIComponent(getCookie("usernick")));
var C_NO_LOGIN = getCookie('no_login') == 1 ? true : false;
var C_LOGIN_AUTO = getCookie('wb_login_auto');

//处理特殊字符，使得jquery选择器可以选择
/*
String.prototype.filter_special = function(){
	return this.replace(/\|/, '\\|');
}
*/
String.prototype.trim = function(){
	return this.replace(/^[\s　]+/gm,"").replace(/[\s ]+$/gm,"");
};

function getFileSize(size)
{
	var kb = 1024; // Kilobyte
	var mb = 1024 * kb; // Megabyte
	var gb = 1024 * mb; // Gigabyte
	return size < kb ? size+'B' : size < mb ? (size / kb).toFixed(2)+'K' : size < gb ? (size / mb).toFixed(2)+'M' : (size / gb).toFixed(2)+'G';
}

function filterName(str)
{
	str = str.replace(/\</g, '&lt;');
	str = str.replace(/\>/g, '&gt');
	str = str.replace(/\:/g, '_');
	str = str.replace(/\"/g, '_');
	str = str.replace(/\|/g, '_');
	str = str.replace(/\*/g, '_');
	str = str.replace(/\?/g, '_');
	str = str.replace(/\//g, '_');
	str = str.replace(/\\/g, '_');
	return str;
}

function getUrlParams() {
	var args = {};
	if (location.search.length > 1)
	{
		var query=location.search.substring(1);
		var pairs=query.split("&");
		for(var i=0; i < pairs.length; i++ ) {
			var pos=pairs[i].indexOf('=');
			if(pos==-1){
				continue;
			}
			var argname=pairs[i].substring(0,pos);
			var value=pairs[i].substring(pos+1);
			try{
				args[argname]=decodeURIComponent(value);
			}catch(e){
				args[argname] = '';
			}
		}
	}
	return args;
}

function sendStat()
{
	try{
		xl_pvManually2.apply(this,arguments);
	}catch(e){}
}

function setXlCookie(name,value,sec){
	if(arguments.length>2){
		var expireDate=new Date(new Date().getTime()+sec*1000);
		document.cookie = name + "=" + escape(value) + "; path=/; domain=xunlei.com; expires=" + expireDate.toGMTString() ;
	}else
	document.cookie = name + "=" + escape(value) + "; path=/; domain=xunlei.com";
}

function closeTopTips(){
	jQuery("#top_tips").slideUp(500);
}

function setWBCookie(name,value,sec){
	if(arguments.length>2){
		var expireDate=new Date(new Date().getTime()+sec*1000);
		document.cookie = name + "=" + encodeURIComponent(value) + "; path=/; domain=yun.vip.xunlei.com; expires=" + expireDate.toGMTString() ;
	}else
	document.cookie = name + "=" + encodeURIComponent(value) + "; path=/; domain=yun.vip.xunlei.com";
}

function getCookie(name){
	return (document.cookie.match(new RegExp("(^"+name+"| "+name+")=([^;]*)"))==null)?"":RegExp.$2;
}

function clear_logout() {
	var ckeys = ["sessionid","userid","usrname","nickname","usernick","isvip","usernewno","no_login"];
	for(var i=0;i<ckeys.length;i++){
		setXlCookie(ckeys[i],"");
	}
	window.location.reload();
}

function logout() {	
	var ckeys = ["sessionid","userid","usrname","nickname","usernick","isvip","usernewno","no_login"];
	for(var i=0;i<ckeys.length;i++){
		setXlCookie(ckeys[i],"");
	}
	setWBCookie("wb_login_auto","2");
	window.location.href = 'http://yun.vip.xunlei.com/login.html';
}

function logout1() {
	var ckeys = ["sessionid","userid","usrname","nickname","usernick","isvip","usernewno","no_login"];
	for(var i=0;i<ckeys.length;i++){
		setXlCookie(ckeys[i],"");
	}
	setWBCookie("wb_login_auto","2");

	$('un_login').style.display = '';
	$('is_login').style.display = 'none';
	$('u').focus();
}

//按需求格式化上传时间
function getFormatTime(ts)
{
	//var month = {1:"Jan",2:"Feb",3:"Mar",4:"Apr",5:"May",6:"Jun",7:"jul",8:"Aug",9:"Sep",10:"Oct",11:"Dec",12:"Nov"};
	ts = ts * 1000;
	var y, m, d, h, i, s, dataTs = new Date(ts),dateNow = new Date(),formatTime,n;
	var ns = dateNow.getTime();
	var diff = ns - ts;
	if(diff < 3600000){
		formatTime = '1小时内';
	}else if(diff >= 3600000 && diff < 3600000*24){
		n = Math.floor(diff/3600000);
		formatTime = n+'小时前';
	}else if(diff >= 3600000*24 && diff < 3600000*24*7){
		n = Math.floor(diff/86400000);
		formatTime = n+'天前';
	}else if(diff >= 3600000*24*7 && diff < 3600000*24*30){
		n = Math.floor(diff/604800000);
		formatTime = n+'周前';
	}else{
		y = dataTs.getFullYear();
	    m = (dataTs.getMonth() + 1);
	    d = dataTs.getDate();
	    h = dataTs.getHours();
	    i = dataTs.getMinutes();
	    i = i < 10 ? ("0" + i) : i;
	    s = dataTs.getSeconds();
	    s = s < 10 ? ("0" + s) : s;
	    formatTime = y + "-" + m + "-" + d + " " + h + ":" + i;
	}
	
	return formatTime;
}

function getScrollTop() {
	var scrollTop=0;
	if(document.documentElement&&document.documentElement.scrollTop)
	{
		scrollTop=document.documentElement.scrollTop;
	} else if(document.body) {
		scrollTop=document.body.scrollTop;
	}else{
		scrollTop = 0;
	}
	return scrollTop;
}

// 得到字符串的真实长度（双字节换算为两个单字节）
function get_string_len(chars)
{
    return chars.replace(/[^\x00-\xff]/g,"xx").length;
}

// 截取固定长度子字符串 source为字符串iLen为长度
function get_substring(source, len)
{
    if(source.replace(/[^\x00-\xff]/g,"xx").length <= len){return source;}
    var str = "";
    var l = 0;
    var schar;
    for(var i=0; schar=source.charAt(i); i++)
    {
        str += schar;
        l += (schar.match(/[^\x00-\xff]/) != null ? 2 : 1);
        if(l >= len) {break;}
    }
    return str;
}

function getSubString(str, len)
{
	return get_string_len(str) > len ? get_substring(str, len - 3) + "..." : str;
}

//过滤html字符
function filter (str) {
	str = str.replace(/&/g, '&amp;');
	str = str.replace(/</g, '&lt;');
	str = str.replace(/>/g, '&gt;');
	str = str.replace(/'/g, '&acute;');
	str = str.replace(/"/g, '&quot;');
	str = str.replace(/\|/g, '&brvbar;');
	return str;
}

//欢迎词
function word_suggest(){
	var n_hour = new Date().getHours();
	var h_suggest = "";
	if( n_hour > 0 && n_hour < 6 ) h_suggest = "凌晨好";
	else if( n_hour >=6 && n_hour < 12 ) h_suggest = "上午好";
	else if( n_hour >=12 && n_hour <18 ) h_suggest = "下午好";
	else h_suggest = "晚上好";
	return h_suggest;
}

//返回文件大小
function get_file_size(size)
{
	var kb = 1024; // Kilobyte
	var mb = 1024 * kb; // Megabyte
	var gb = 1024 * mb; // Gigabyte
	return size < kb ? size+'B' : size < mb ? (size / kb).toFixed(2)+'K' : size < gb ? (size / mb).toFixed(2)+'M' : (size / gb).toFixed(2)+'G';
}

//获取所需下载时间
function get_need_time(seconds)
{
	var hour = seconds >= 3600  ? Math.round(seconds / 3600) : 0;
	var remain = seconds % 3600;
	var min = remain >= 60 ? Math.round(remain / 60) : 0;
	remain = remain % 60;
	return hour > 99 ? '大于100小时' : (hour > 0 ? (hour < 10 ? '0'+hour : hour) : '00')+':'+(min > 0 ? (min < 10 ? '0'+min : min) : '00')+':'+(remain > 0 ? (remain < 10 ? '0'+remain : remain) : '00');
}

//弹出层并自动滚动
function showPop(id, callback){
	var top = 0, bottom = -1;
	if(id == "totalProgressPop"){
		top = 49;
		bottom = 0;
	}else if(id == "toolTipsPop"){
		top = 100;
	}
	if(id == "uploadListPop"){
		var bodyObj = document.documentElement;
		var jqPopDiv = $("#uploadListPop");
		if(document.compatMode == "BackCompat"){
			bodyObj = document.body;
		}
	
		//var dleft = bodyObj.clientWidth/2 - jqPopDiv.width()/2;
		//var dtop = top == 0 ? bodyObj.clientHeight/2 - jqPopDiv.height()/2 : top;
		var dright = 0;
		var dbottom = 0;

		    //dleft += bodyObj.scrollLeft + document.body.scrollLeft;
			dright += bodyObj.scrollLeft + document.body.scrollLeft;
		if(isIE6){
			dbottom += bodyObj.scrollTop + document.body.scrollTop;
		}
		var scrollTop = parseInt(document.documentElement.scrollTop), screenTop = document.documentElement.clientHeight,
		body = document.body.clientHeight;
		screenTop = screenTop + scrollTop;
		if(screenTop > body){
			screenTop = body;
		}
		//jqPopDiv.hide().css({left:dleft,top:screenTop});
		//jqPopDiv.show().animate({left:dleft,top:dtop}, 800);
	    jqPopDiv.hide().css({right:dright,bottom:scrollTop});
	    jqPopDiv.show().css({right:dright,bottom:dbottom});

	}else{
		switch(id)
		{
			case "upload_sel_list_pop":
				$("#"+id).show();
				break;
			default:
				$("#"+id).show();
		}
		set_div_pos($("#"+id), top, bottom);
	}

	typeof callback == "function" ? callback() : null;
	if(isIE6){
		setTimeout(function(){
			set_div_pos($("#"+id), top, bottom);
		},10);
		if(typeof window["resize_"+id] == "undefined"){
			window["resize_"+id] = function(){
				setTimeout(function(){
					set_div_pos($("#"+id), top, bottom);
				},10);
			}
		}
		$(window).bind("resize", window["resize_"+id]).bind("scroll", window["resize_"+id]);
	}
}

//关闭弹出层等
function hidePop(id,noProgress){
	if(isIE6 && window["resize_" + id]){
		$(window).unbind("resize", window["resize_"+id]).unbind("scroll", window["resize_"+id]);
	}
	
	switch(id){
		case "uploadListPop":
			var objs = $("#uploadQueue li");
			objs.each(function(){
				var obj  =$(this), size = obj.find(".size").val(), status = obj.find(".status").val();
				if(status == 0){
					Upload.uploadSize -= size;
					Upload.uploadSize = Upload.uploadSize >= 0 ? Upload.uploadSize : 0;
					Upload.loadedSize -= size;
					Upload.loadedSize = Upload.loadedSize > 0 ? Upload.loadedSize : 0;
					obj.remove();
				}
			});
			$("#"+id).hide();
			if(!noProgress){
				showPop('totalProgressPop');
			}else{
				hidePop('totalProgressPop');
			}
			break;
		case "moveFilePop":
			$("#mimyDisk").attr("class", "ic_small ic_hidflow");
			$("#mamyDisk").removeClass("on");
			$("#mlimyDisk").children("ul").remove();
			$("#"+id).hide();
			break;
		case "newDirPop":
			if($("#mlimyDisk a[class=on]").length > 0){
				$("#moveFilePop").show();
			}
			$("#"+id).hide();
			break;
		default :
			$("#"+id).hide();
			break;
	}
}

function set_div_pos(jqPopDiv, top, bottom)
{
	var bodyObj = document.documentElement;
	if(document.compatMode == "BackCompat"){
		bodyObj = document.body;
	}
	var dleft = bodyObj.clientWidth/2 - jqPopDiv.width()/2;
	var dtop = top == 0 ? bodyObj.clientHeight/2 - jqPopDiv.height()/2 : top;

	dleft += bodyObj.scrollLeft + document.body.scrollLeft;
	if(isIE6){
		dtop += bodyObj.scrollTop + document.body.scrollTop;
	}

	if(bottom != -1){
		var scrollTop = parseInt(document.documentElement.scrollTop), screenTop = document.documentElement.clientHeight,
		body = document.body.clientHeight;
		screenTop = screenTop + scrollTop;
		if(screenTop > body){
			screenTop = body;
		}
		jqPopDiv.css({left:dleft,top:screenTop - 50});
	}else{
		jqPopDiv.css({left:dleft,top:dtop});
	}
	
}

//thunder download

function BigThunder()
{
    this.$();
}
BigThunder.prototype = {
    $:
        function ()
        {
            if (typeof(this._o) == 'undefined')
                try {
                    BigThunder.prototype._o = new ActiveXObject("ThunderAgent.Agent");
                } catch (e) {
                    try {
                        BigThunder.prototype._o = new ActiveXObject("ThunderAgent.Agent.1");
                    } catch (e) {
                        BigThunder.prototype._o = null;
                    }
                }

            return this._o;
        },
    _af: -1,
    _afs:
        [
           // function (t, c, u, r, n, s) { t.AddTask5(u, "", "", n, r, -1, 0, -1, "", c, s, 1, "", -1) },
            function (t, c, u, r, n, s) {t.AddTask5(u, n, "",n,r, -1, 0, 4, "", c, s, 1, "", -1)},
            function (t, c, u, r, n, s) {t.AddTask4(u, "", "", n, r, -1, 0, -1, "", c, s)},
            function (t, c, u, r, n, s) {t.AddTask3(u, "", "", n, r, -1, 0, -1, "", c)},
            function (t, c, u, r, n, s) {t.AddTask2(u, "", "", n, r, -1, 0, -1, "")},
            function (t, c, u, r, n, s) {t.AddTask (u, "", "", n, r, -1, 0, -1)}
        ],
    _add:
        function (cid, url, refer, name, stat)
        {
            if (this._af >= 0) {
                this._afs[this._af](this._o, cid, url, refer, name, stat);
                return true;
            }
            for (var i = 0; i < this._afs.length; ++i)
                try {
                    this._afs[i](this._o, cid, url, refer, name, stat);
                    this._af = i;
                    return true;
                } catch (e) {
                }
            return false;
        },
    _cf: -1,
    _cfs:
        [
            function (t) {t.CommitTasks2(1);},
            function (t) {t.CommitTasks();}
        ],
    _commit:
        function ()
        {
            if (this._cf >= 0) {
                this._cfs[this._cf](this._o);
                return true;
            }
            for (var i = 0; i < this._cfs.length; ++i)
                try {
                    this._cfs[i](this._o);
                    this._cf = i;
                    return true;
                } catch (e) {
                }
            return false;
        },
    down:
        function (cid, url, refer, name, stat)
        {
            if (!(this._add(cid, url, refer, name, stat) && this._commit()))
                throw '不支持此方法，请安装最新的迅雷客户端';
        },
    batch:
        function (res)
        {
            for (var i = 0; i < res.length; ++i) {
                var o = res[i];
                if (!this._add(o.cid, o.url, o.refer, o.name, o.stat))
                    return false;
            }
            return this._commit();
        }
};
function WebThunder()
{
    this.$();
}
WebThunder.prototype = {
    $:
        function ()
        {
            if (typeof(this._o) == 'undefined')
                try {
                    WebThunder.prototype._o = new ActiveXObject("ThunderServer.webThunder.1");
                } catch (e) {
                    WebThunder.prototype._o = null;
                }
            return this._o;
        },
    _whd:
        function (s)
        {
            return s.replace(/<br>/g,"\n").replace(/&lt/g,"<").replace(
                /&gt/g,">"
            ).replace(/&quot/g,"\"").replace(/&apos/g,"\'").replace(/&amp/g,"&");
        },
    down:
        function (cid, url, refer, name, stat)
        {
            this._o.CallAddTask(this._whd(url), name, this._whd(refer), 1, cid, stat);
        },
    batch:
        function (res)
        {
            var s = this._o.BeginBatchTask();
            for (var i = 0; i < res.length; ++i) {
                var o = res[i];
                this._o.AddTaskToBatch(s, o.url, o.name, o.refer, o.cid, o.stat);
            }
            this._o.EndBatchTask(s);
        }
};
function Thunder(web_first, only)
{
    this.$(web_first, only);
}
Thunder.prototype = {
    $:
        function (web_first, only)
        {
            if (typeof(this._$) == 'undefined') {
                this.web_first = web_first;
                this.only = typeof(only) == 'undefined' ? false : only;
                this._o = web_first ? new WebThunder() : new BigThunder();
                if (this._o.$())
                    this._$ = true;
                else if (only)
                    this._$ = false;
                else {
                    this._o = web_first ? new BigThunder() : new WebThunder();
                    this._$ = this._o.$() ? true : false;
                }
            }
            return this._$;
        },
    down:
        function (cid, url, ref, nam, sta)
        {
            if (!this._$)
                return true;
            this._o.down(cid, url, ref, nam, sta);
            return false;
        },
    batch:
        function (res)
        {
            if (!this._$)
                return true;
            this._o.batch(res);
            return false;
        }
};

// Simple Set Clipboard System
// Author: Joseph Huckaby

var ZeroClipboard = {

	version: "1.0.7",
	clients: {}, // registered upload clients on page, indexed by id
	moviePath: 'ZeroClipboard.swf', // URL to movie
	nextId: 1, // ID of next movie

	$: function(thingy) {
		// simple DOM lookup utility function
		if (typeof(thingy) == 'string') thingy = document.getElementById(thingy);
		if (!thingy.addClass) {
			// extend element with a few useful methods
			thingy.hide = function() {this.style.display = 'none';};
			thingy.show = function() {this.style.display = '';};
			thingy.addClass = function(name) {this.removeClass(name);this.className += ' ' + name;};
			thingy.removeClass = function(name) {
				var classes = this.className.split(/\s+/);
				var idx = -1;
				for (var k = 0; k < classes.length; k++) {
					if (classes[k] == name) {idx = k;k = classes.length;}
				}
				if (idx > -1) {
					classes.splice( idx, 1 );
					this.className = classes.join(' ');
				}
				return this;
			};
			thingy.hasClass = function(name) {
				return !!this.className.match( new RegExp("\\s*" + name + "\\s*") );
			};
		}
		return thingy;
	},

	setMoviePath: function(path) {
		// set path to ZeroClipboard.swf
		this.moviePath = path;
	},

	dispatch: function(id, eventName, args) {
		// receive event from flash movie, send to client
		var client = this.clients[id];
		if (client) {
			client.receiveEvent(eventName, args);
		}
	},

	register: function(id, client) {
		// register new client to receive events
		this.clients[id] = client;
	},

	getDOMObjectPosition: function(obj, stopObj) {
		// get absolute coordinates for dom element
		var info = {
			left: 0,
			top: 0,
			width: obj.width ? obj.width : obj.offsetWidth,
			height: obj.height ? obj.height : obj.offsetHeight
		};

		while (obj && (obj != stopObj)) {
			info.left += obj.offsetLeft;
			info.top += obj.offsetTop;
			obj = obj.offsetParent;
		}

		return info;
	},

	Client: function(elem) {
		// constructor for new simple upload client
		this.handlers = {};

		// unique ID
		this.id = ZeroClipboard.nextId++;
		this.movieId = 'ZeroClipboardMovie_' + this.id;

		// register client with singleton to receive flash events
		ZeroClipboard.register(this.id, this);

		// create movie
		if (elem) this.glue(elem);
	}
};
ZeroClipboard.Client.prototype = {
	id: 0, // unique ID for us
	ready: false, // whether movie is ready to receive events or not
	movie: null, // reference to movie object
	clipText: '', // text to copy to clipboard
	handCursorEnabled: true, // whether to show hand cursor, or default pointer cursor
	cssEffects: true, // enable CSS mouse effects on dom container
	handlers: null, // user event handlers
	glue: function(elem, appendElem, stylesToAdd) {
		// glue to DOM element
		// elem can be ID or actual DOM element object
		this.domElement = ZeroClipboard.$(elem);
		// float just above object, or zIndex 99 if dom element isn't set
		var zIndex = 99;
		if (this.domElement.style.zIndex) {
			zIndex = parseInt(this.domElement.style.zIndex, 10) + 1;
		}
		if (typeof(appendElem) == 'string') {
			appendElem = ZeroClipboard.$(appendElem);
		}
		else if (typeof(appendElem) == 'undefined') {
			appendElem = document.getElementsByTagName('body')[0];
		}

		// find X/Y position of domElement
		var box = ZeroClipboard.getDOMObjectPosition(this.domElement, appendElem);

		// create floating DIV above element
		this.div = document.createElement('div');
		var style = this.div.style;
		style.position = 'absolute';
		style.left = '' + box.left + 'px';
		style.top = '' + box.top + 'px';
		style.width = '' + box.width + 'px';
		style.height = '' + box.height + 'px';
		style.zIndex = zIndex;

		if (typeof(stylesToAdd) == 'object') {
			for (addedStyle in stylesToAdd) {
				style[addedStyle] = stylesToAdd[addedStyle];
			}
		}

		// style.backgroundColor = '#f00'; // debug

		appendElem.appendChild(this.div);

		this.div.innerHTML = this.getHTML( box.width, box.height );
	},

	getHTML: function(width, height) {
		// return HTML for movie
		var html = '';
		var flashvars = 'id=' + this.id +
			'&width=' + width +
			'&height=' + height;

		if (navigator.userAgent.match(/MSIE/)) {
			// IE gets an OBJECT tag
			var protocol = location.href.match(/^https/i) ? 'https://' : 'http://';
			html += '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="'+protocol+'download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'+width+'" height="'+height+'" id="'+this.movieId+'" align="middle"><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="false" /><param name="movie" value="'+ZeroClipboard.moviePath+'" /><param name="loop" value="false" /><param name="menu" value="false" /><param name="quality" value="best" /><param name="bgcolor" value="#ffffff" /><param name="flashvars" value="'+flashvars+'"/><param name="wmode" value="transparent"/></object>';
		}
		else {
			// all other browsers get an EMBED tag
			html += '<embed id="'+this.movieId+'" src="'+ZeroClipboard.moviePath+'" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="'+width+'" height="'+height+'" name="'+this.movieId+'" align="middle" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="'+flashvars+'" wmode="transparent" />';
		}
		return html;
	},

	hide: function() {
		// temporarily hide floater offscreen
		if (this.div) {
			this.div.style.left = '-2000px';
		}
	},

	show: function() {
		// show ourselves after a call to hide()
		this.reposition();
	},

	destroy: function() {
		// destroy control and floater
		if (this.domElement && this.div) {
			this.hide();
			this.div.innerHTML = '';

			var body = document.getElementsByTagName('body')[0];
			try {body.removeChild( this.div );} catch(e) {;}

			this.domElement = null;
			this.div = null;
		}
	},

	reposition: function(elem) {
		// reposition our floating div, optionally to new container
		// warning: container CANNOT change size, only position
		if (elem) {
			this.domElement = ZeroClipboard.$(elem);
			if (!this.domElement) this.hide();
		}

		if (this.domElement && this.div) {
			var box = ZeroClipboard.getDOMObjectPosition(this.domElement);
			var style = this.div.style;
			style.left = '' + box.left + 'px';
			style.top = '' + box.top + 'px';
		}
	},

	setText: function(newText) {
		// set text to be copied to clipboard
		this.clipText = newText;
		if (this.ready) this.movie.setText(newText);
	},

	addEventListener: function(eventName, func) {
		// add user event listener for event
		// event types: load, queueStart, fileStart, fileComplete, queueComplete, progress, error, cancel
		eventName = eventName.toString().toLowerCase().replace(/^on/, '');
		if (!this.handlers[eventName]) this.handlers[eventName] = [];
		this.handlers[eventName].push(func);
	},

	setHandCursor: function(enabled) {
		// enable hand cursor (true), or default arrow cursor (false)
		this.handCursorEnabled = enabled;
		if (this.ready) this.movie.setHandCursor(enabled);
	},

	setCSSEffects: function(enabled) {
		// enable or disable CSS effects on DOM container
		this.cssEffects = !!enabled;
	},

	receiveEvent: function(eventName, args) {
		// receive event from flash
		eventName = eventName.toString().toLowerCase().replace(/^on/, '');

		// special behavior for certain events
		switch (eventName) {
			case 'load':
				// movie claims it is ready, but in IE this isn't always the case...
				// bug fix: Cannot extend EMBED DOM elements in Firefox, must use traditional function
				this.movie = document.getElementById(this.movieId);
				if (!this.movie) {
					var self = this;
					setTimeout( function() {self.receiveEvent('load', null);}, 1 );
					return;
				}

				// firefox on pc needs a "kick" in order to set these in certain cases
				if (!this.ready && navigator.userAgent.match(/Firefox/) && navigator.userAgent.match(/Windows/)) {
					var self = this;
					setTimeout( function() {self.receiveEvent('load', null);}, 100 );
					this.ready = true;
					return;
				}

				this.ready = true;
				this.movie.setText( this.clipText );
				this.movie.setHandCursor( this.handCursorEnabled );
				break;

			case 'mouseover':
				if (this.domElement && this.cssEffects) {
					this.domElement.addClass('hover');
					if (this.recoverActive) this.domElement.addClass('active');
				}
				break;

			case 'mouseout':
				if (this.domElement && this.cssEffects) {
					this.recoverActive = false;
					if (this.domElement.hasClass('active')) {
						this.domElement.removeClass('active');
						this.recoverActive = true;
					}
					this.domElement.removeClass('hover');
				}
				break;

			case 'mousedown':
				if (this.domElement && this.cssEffects) {
					this.domElement.addClass('active');
				}
				break;

			case 'mouseup':
				if (this.domElement && this.cssEffects) {
					this.domElement.removeClass('active');
					this.recoverActive = false;
				}
				break;
		} // switch eventName

		if (this.handlers[eventName]) {
			for (var idx = 0, len = this.handlers[eventName].length; idx < len; idx++) {
				var func = this.handlers[eventName][idx];

				if (typeof(func) == 'function') {
					// actual function reference
					func(this, args);
				}
				else if ((typeof(func) == 'object') && (func.length == 2)) {
					// PHP style object + method, i.e. [myObject, 'myMethod']
					func[0][ func[1] ](this, args);
				}
				else if (typeof(func) == 'string') {
					// name of function
					window[func](this, args);
				}
			} // foreach event handler defined
		} // user defined handler for event
	}
};

function SerializeJsonToStr(oJson)
{
	if( oJson == null )
		return "null";
	if( typeof(oJson) == typeof(0) )
		return oJson.toString();
	if( typeof(oJson) == typeof('') ||
		oJson instanceof String )
		{
		oJson = oJson.toString();
		oJson = oJson.replace( /\r\n/, '\\r\\n');
		oJson = oJson.replace( /\n/, '\\n');
		oJson = oJson.replace( /'/, '\'');
		return '"' + oJson + '"';
	}
	if( oJson instanceof Array )
	{
		var strRet = "[";
		for( var i = 0; i < oJson.length; i++)
		{
			if( strRet.length > 1 )
				strRet += ",";
			strRet += SerializeJsonToStr(oJson[i]);
		}
		strRet += "]";
		return strRet;
	}
	if( typeof(oJson) == typeof({}) )
	{
		var strRet = "{";
		for( var p in oJson )
		{
			if( strRet.length > 1 )
				strRet += ",";
			strRet += '"'+p.toString() + '":' + SerializeJsonToStr(oJson[p]);
		}
		strRet += "}";
		return strRet;
	}
}

function haslogin(){
	var sessionid = getCookie("sessionid");
	if(sessionid== null){sessionid="";}
	var vip_sessionid = getCookie("vip_sessionid");
	if(vip_sessionid== null){vip_sessionid="";}
	if(vip_sessionid == "-1" || sessionid.length < 32){
		return false;
	}
	return true;
}
function delCookie(name){
    var date = new Date();
    date.setTime(date.getTime() - 100000);
    document.cookie = name + "=a; domain=.xunlei.com;path=/;expires=" + date.toGMTString();
}