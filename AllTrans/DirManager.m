//
//  DirManager.m
//  FlyTransfer
//
//  Created by apple on 13-8-28.
//  Copyright (c) 2013年 Calibri. All rights reserved.
//

#import "DirManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "DirInfo.h"
#import "utility.h"
#import "Statistics.h"

NSMutableDictionary	*dirMap;
NSMutableDictionary *uploadedMap;
NSMutableSet *uploadingSet;

NSOperationQueue *g_saveQueue;
//NSLock* picLock = nil;

NSObject* lockForSync;
int isSync;


@implementation DirManager

+ (NSMutableDictionary*)getDirMap
{
	return dirMap;
}

+ (NSMutableDictionary*)getUploadedMap
{
    return uploadedMap;
}

+ (void)initDirMap
{
    dirMap = [[NSMutableDictionary alloc] init];
    uploadedMap = [[NSMutableDictionary alloc] init];
    uploadingSet = [[NSMutableSet alloc] init];
    g_saveQueue = [[NSOperationQueue alloc] init];
//    picLock = [[NSLock alloc] init];
    isSync = 0;
    return;
}

+ (int)loadResources
{	
	static DirInfo* sp_cDirInfo;
	
//    if (![picLock tryLock]) {
//        return -1;
//    }

    if (isSync != 0) {
        return -1;
    }
    
	void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
		if(result != NULL) {
			[sp_cDirInfo.assets addObject:result];
			//NSFileManager *fileManager = [NSFileManager defaultManager];
            
			
			
			/*NSString* fileName = [[NSString alloc] initWithFormat:@"%d", sp_cDirInfo.fileCount];
             fileName = [sp_cDirInfo.dirPath stringByAppendingPathComponent:fileName];
             NSLog(@"save file:%@",fileName);
             [fileManager createFileAtPath:fileName contents:nil attributes:nil];
             NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:fileName];
             if (!handle)
             {
             }
             
             static const NSUInteger BufferSize = 1024*1024;
             ALAssetRepresentation *rep = [result defaultRepresentation];
             uint8_t *buffer = calloc(BufferSize, sizeof(*buffer));
             NSUInteger offset = 0, bytesRead = 0;
             
             // Read the buffer and write the data to your destination path as you go
             do
             {
             @try
             {   
             bytesRead = [rep getBytes:buffer fromOffset:offset length:BufferSize error:nil];
             [handle writeData:[NSData dataWithBytesNoCopy:buffer length:bytesRead freeWhenDone:NO]];
             offset += bytesRead;
             }
             @catch (NSException *exception)
             {
             free(buffer);
             
             // Handle the exception here...
             }
             } while (bytesRead > 0);
             free(buffer);
             [handle closeFile];*/
			
            //			UIImage* img = [[UIImage alloc] initWithContentsOfFile: fileName];
			
            //			NSString * thumbfileName = [[NSString alloc] initWithFormat:@"%d", sp_cDirInfo.fileCount];
            
            //			UIImage *thumbnail = [img imageScaleAndCropToMaxSize:CGSizeMake(75,75)];
            //			NSData *jpg = UIImageJPEGRepresentation(thumbnail, 0.8);
			
            //			thumbfileName = [sp_cDirInfo.thumbPath stringByAppendingPathComponent:thumbfileName];
			//NSLog(@"save thumbnail :%@",thumbfileName);
			
            //			[jpg writeToFile:thumbfileName atomically:NO];
			
			//need notification
            
			sp_cDirInfo.fileCount++;
		}
	};
	
	void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
		NSFileManager *defaultManager = [NSFileManager defaultManager];
        
		if(group != nil) {
			
			DirInfo* cDirInfo = [[DirInfo alloc] init];
			cDirInfo.fileCount = 0;
			
			cDirInfo.dirName = [group valueForProperty:ALAssetsGroupPropertyName];
			
			NSString * documentsDir = [utility getDocumentSubPath:@"Picture"];
			
			cDirInfo.dirPath = [documentsDir stringByAppendingPathComponent:cDirInfo.dirName];
			//NSLog(@"current dir: %@", cDirInfo.dirPath);
			
            //			cDirInfo.thumbPath = [[utility getDocumentSubPath:@"thumbnails"] stringByAppendingPathComponent:cDirInfo.dirName];
			
            NSMutableDictionary* dirMap = [DirManager getDirMap];
			[dirMap setValue:cDirInfo forKey:cDirInfo.dirName];
			sp_cDirInfo = cDirInfo;
			
			if(![defaultManager fileExistsAtPath:sp_cDirInfo.dirPath])
			{
				[defaultManager createDirectoryAtPath:sp_cDirInfo.dirPath withIntermediateDirectories:YES attributes:nil error:nil];
			}
			[group enumerateAssetsUsingBlock:assetEnumerator];
			
            //			NSMutableArray *fileList = [NSMutableArray arrayWithArray:
            //								[defaultManager contentsOfDirectoryAtPath:sp_cDirInfo.dirPath error:nil]];
			
            //			int delNum = [fileList count] - sp_cDirInfo.fileCount;
			
			//NSLog(@"dir==%@,fileList%@,delfilenum=%d",sp_cDirInfo.dirPath,fileList, delNum);
            
            //			if (fileList != nil && delNum>0) {
            
            //				for (; delNum != 0; delNum--) {
            //					NSString* delName = [[NSString alloc] initWithFormat:@"%d", sp_cDirInfo.fileCount-delNum+1];
            //					NSLog(@"delfile==%@",delName);
            //					NSString* strDir = [sp_cDirInfo.dirPath stringByAppendingPathComponent:delName];
            //					[defaultManager removeItemAtPath:strDir error:nil];
            //					strDir = [sp_cDirInfo.thumbPath stringByAppendingPathComponent:delName];
            //					[defaultManager removeItemAtPath:strDir error:nil];
            //				}
            //			}			
			//need notification
		}else {
			NSString* picdir = [utility getDocumentSubPath:@"Picture"];
			NSMutableArray *fileList = [NSMutableArray arrayWithArray:
										[[NSFileManager defaultManager] contentsOfDirectoryAtPath:picdir error:nil]];
            NSMutableDictionary* dirMap = [DirManager getDirMap];
			for (NSString* elem in fileList) {
				if ([dirMap objectForKey:elem] == nil && ![elem isEqualToString:@"MyUpload"])
				{
					NSLog(@"deldir==%@",elem);
					NSString* delpath = [picdir stringByAppendingPathComponent:elem];
					[defaultManager removeItemAtPath:delpath error:nil];
				}
			}
//			[picLock unlock];

			NSLog(@"all finished");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Picture" object:nil];
            
		}
        
	};
	
	[dirMap removeAllObjects];
	ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
	[library enumerateGroupsWithTypes:ALAssetsGroupAlbum|ALAssetsGroupSavedPhotos
						   usingBlock:assetGroupEnumerator
						 failureBlock: ^(NSError *error) {
							 NSLog(@"Failure");
						 }];
    
    return 0;
}


+ (void)savePhotoCall:(id)data {
    NSAssert(data != nil, @"Unassigned NSArray for data.");
    
    UIImage *photo = (UIImage *)[data objectAtIndex:0];
//    NSLog(@"%@",picLock);
    @synchronized(lockForSync)
    {
        isSync++;
        NSLog(@"isSync plus");
    }
    

//    [picLock lock];

    NSLog(@"saving");

    UIImageWriteToSavedPhotosAlbum(photo, [DirManager class], @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

+ (void)savePhoto:(UIImage *)photo{
    NSArray *data = [NSArray arrayWithObjects:photo, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"4photosync" object:nil];

    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:[DirManager class]
                                                                            selector:@selector(savePhotoCall:)
                                                                              object:data];
    [g_saveQueue addOperation:operation];
    [operation release];
}

+ (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSLog(@"finished %@", contextInfo);
    [image release];
    @synchronized(lockForSync)
    {
        isSync--;
        NSLog(@"isSync minus");
        if (isSync == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"4photosyncover" object:nil];
        }
    }
//    NSString* lastObj = (NSString*)contextInfo;
//    NSString* filename = [[utility getDocumentSubPath:@"MyUpload"] stringByAppendingPathComponent:lastObj];
//    NSLog(@"saved file to album:%@", filename);

//    [lastObj release];

//    [[NSFileManager defaultManager] removeItemAtPath:filename error:nil];

// Was there an error?
//    NSLog(@"unlock");

//    [picLock unlock];
    if (error != NULL)
    {
        NSLog(@"an error:%@", error);

    }
    else  // No errors
    {
// Show message image successfully saved
        NSLog(@"save success");
//        [[NSNotificationCenter defaultCenter] postNotificationName:notificationPostName object:nil];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"4tab" object:nil];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"4tab" object:nil];
//    [DirManager loadResources];
}

+ (bool)isSync
{
    return (isSync > 0);
}

@end
