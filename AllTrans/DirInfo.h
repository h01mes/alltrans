//
//  DirInfo.h
//  FlyTransfer
//
//  Created by  apple on 13-8-19.
//  Copyright 2013 Calibri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KTPhotoBrowserDataSource.h"

NSMutableSet	*uploadSet;


@interface DirInfo : NSObject<KTPhotoBrowserDataSource> 
{
	NSString* dirPath;
	NSString* dirName;
	NSString* thumbPath;
	int fileCount;
	NSMutableArray* assets;
}

@property (nonatomic, retain) NSString *dirPath;
@property (nonatomic, retain) NSString *dirName;
@property (nonatomic, retain) NSString *thumbPath;
@property (nonatomic, retain) NSMutableArray* assets;

@property (assign, nonatomic) int fileCount;

- (NSInteger)numberOfPhotos;

- (UIImage *)imageAtIndex:(NSInteger)index;
- (UIImage *)thumbImageAtIndex:(NSInteger)index;

@end