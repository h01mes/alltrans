//
//  appwhichAppDelegate.h
//  Altrans
//
//  Created by Holmes on 1/2/14.
//  Copyright (c) 2014 Holmes. All rights reserved.
//

#import <UIKit/UIKit.h>


@class HTTPServer;
@interface appwhichAppDelegate : UIResponder <UIApplicationDelegate>
{
    HTTPServer      *webServer;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *viewController;

#pragma mark - HTTPSERVER

- (void)initHttpServer;
- (void)startHttpServer;
- (void)stopHttpServer;

@end
