//
//  PdfController.h
//  flytransfer
//
//  Created by apple on 13-10-29.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationalSubView.h"

@class PDFScrollView;

@interface PdfController : NavigationalSubView
{
    NSURL * _url;
}

@property (nonatomic, retain) NSURL * _url;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil url:(NSURL*) url;

@end
