//
//  DirInfo.m
//  FlyTransfer
//
//  Created by  apple on 13-8-19.
//  Copyright 2013 Calibri. All rights reserved.
//

#import "DirInfo.h"
#import <AssetsLibrary/AssetsLibrary.h>


@implementation DirInfo

@synthesize dirPath;
@synthesize dirName;
@synthesize fileCount;
@synthesize thumbPath;
@synthesize assets;

- (id) init{
	self = [super init];
	if (self != nil) {
		assets = [[NSMutableArray alloc] init];
	}
	return self;
}

- (NSInteger)numberOfPhotos{
	return fileCount;
}

- (UIImage *)imageAtIndex:(NSInteger)index
{
	ALAsset* thisasset = [assets objectAtIndex:fileCount-1-index];
	ALAssetRepresentation *rep = [thisasset defaultRepresentation];
	return [UIImage imageWithCGImage:[rep fullResolutionImage]];
}

- (UIImage *)thumbImageAtIndex:(NSInteger)index
{
	ALAsset* thisasset = [assets objectAtIndex:fileCount-1-index];
	return [UIImage imageWithCGImage:[thisasset thumbnail]];
}

@end