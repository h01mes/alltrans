//
//  appwhichAppDelegate.m
//  Altrans
//
//  Created by Holmes on 1/2/14.
//  Copyright (c) 2014 Holmes. All rights reserved.
//

#import "appwhichAppDelegate.h"
#import "UIImage+KTCategory.h"
#import "utility.h"
#import "DirInfo.h"
#import "DirManager.h"
#import "HTTPServer.h"
#import "WTZHTTPConnection.h"
#import "ExplorerViewController.h"
#import "Statistics.h"
#import <UIKit/UIKit.h>

#import "UINavigationControllerNoRotate.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "ToPCViewController.h"
#import "FinderViewController.h"
#import "PhotoController.h"
#import "SettingController.h"


@implementation appwhichAppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSLog(@"%f, %f", [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);

    self.window.backgroundColor = [UIColor whiteColor];
    [self setupViewControllers];

    [self.window setRootViewController:self.viewController];
    [self.window makeKeyAndVisible];
    
    [self customizeInterface];
    
    [DirManager initDirMap];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSString * documentsDir = [utility getDocumentPath];
    //	if([fileManager fileExistsAtPath:documentsDir])
    //		[fileManager removeItemAtPath:documentsDir error:nil];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* currentVersion = NSLocalizedString(@"supportrow3value", @"");
    NSString* lastVersion = [userDefaults valueForKey:@"version"];
    NSString *resourceFolderPath = [[NSBundle mainBundle] pathForResource:@"web" ofType:nil];

    if (!lastVersion) {
        [fileManager copyItemAtPath:resourceFolderPath toPath:documentsDir error:nil];
    }
    else if (![lastVersion isEqualToString:currentVersion])
    {
        // CALL your Function;
        
        // Adding version number to NSUserDefaults for first version:
        
		NSLog(@"%@", resourceFolderPath);

        [utility truncateResourceAtDir:documentsDir];
        [utility copyResourceAtDir:documentsDir from:resourceFolderPath];
    }
    [userDefaults setObject:currentVersion forKey:@"version"];
	
    if(![fileManager fileExistsAtPath:[utility getDocumentSubPath:@"MyUpload"]])
        [fileManager createDirectoryAtPath:[utility getDocumentSubPath:@"MyUpload"] withIntermediateDirectories:YES attributes:nil error:nil];
    
    if(![fileManager fileExistsAtPath:[utility getDocumentSubPath:@"thumbnails"]])
        [fileManager createDirectoryAtPath:[utility getDocumentSubPath:@"thumbnails"] withIntermediateDirectories:YES attributes:nil error:nil];
    
    if(![fileManager fileExistsAtPath:[utility getDocumentSubPath:@"Video"]])
        [fileManager createDirectoryAtPath:[utility getDocumentSubPath:@"Video"] withIntermediateDirectories:YES attributes:nil error:nil];
    
    if(![fileManager fileExistsAtPath:[utility getDocumentSubPath:@"Music"]])
        [fileManager createDirectoryAtPath:[utility getDocumentSubPath:@"Music"] withIntermediateDirectories:YES attributes:nil error:nil];
    
    if(![fileManager fileExistsAtPath:[utility getDocumentSubPath:@"Other"]])
        [fileManager createDirectoryAtPath:[utility getDocumentSubPath:@"Other"] withIntermediateDirectories:YES attributes:nil error:nil];
    
	
    
    // Override point for customization after application launch.
	//[NSThread detachNewThreadSelector:@selector(loadResources) toTarget:[DirManager class] withObject:nil];
    [DirManager loadResources];
    [self initHttpServer];
    [self startHttpServer];
    
    if ([userDefaults integerForKey:@"INSTALL_REPORT"] != 1) {
        [Statistics reportInstall];
    }
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
//    [Statistics reportForeground];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    [self stopHttpServer];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    NSString * tmpDir = [utility getDocumentSubPath:@"tmp"];
    NSString * uploadDir = [utility getDocumentSubPath:@"MyUpload"];
    NSString * videoThumbDir = [utility getDocumentSubPath:@"thumbnails"];
    
    [utility truncateDir:tmpDir];
    [utility truncateDir:uploadDir];
    [utility truncateDir:videoThumbDir];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
	[self startHttpServer];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [DirManager loadResources];
    
//    [Statistics reportForeground];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}



#pragma mark - HTTPSERVER

- (void)initHttpServer
{
    if (webServer == nil) {
        webServer = [[HTTPServer alloc] init];
        webServer.port = 8080;
		//        webServer.documentRoot = [[NSBundle mainBundle] pathForResource:@"web" ofType:nil];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		
        webServer.documentRoot = [[paths objectAtIndex:0] stringByAppendingString:@"/web"];
        webServer.type = @"_http._tcp.";
        webServer.connectionClass = [WTZHTTPConnection class];
    }
}


- (void)startHttpServer
{
    if (webServer != nil && ![webServer isRunning]) {
        [webServer start:nil];
    }
}

- (void)stopHttpServer
{
    if (webServer != nil && [webServer isRunning]) {
        [webServer stop];
    }
}

#pragma mark - Tab Bar Methods

- (void)setupViewControllers {
    UIViewController *firstViewController = [[ToPCViewController alloc] init];
    UIViewController *firstNavigationController = [[UINavigationControllerNoRotate alloc]
                                                   initWithRootViewController:firstViewController];
    
    UIViewController *secondViewController = [[FinderViewController alloc] init];
    UIViewController *secondNavigationController = [[UINavigationControllerNoRotate alloc]
                                                    initWithRootViewController:secondViewController];
    
//    UIViewController *secondViewController = [[PhotoController alloc] init];
//    UIViewController *secondNavigationController = [[UINavigationControllerNoRotate alloc]
//                                                        initWithRootViewController:secondViewController];
    
    UIViewController *thirdViewController = [[SettingController alloc] initWithStyle:UITableViewStyleGrouped];
    UIViewController *thirdNavigationController = [[UINavigationControllerNoRotate alloc]
                                                    initWithRootViewController:thirdViewController];

    
    RDVTabBarController *tabBarController = [[RDVTabBarController alloc] init];
    [tabBarController setViewControllers:@[firstNavigationController, secondNavigationController, thirdNavigationController]];
    self.viewController = tabBarController;
    
    [self customizeTabBarForController:tabBarController];
}

- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
    UIImage *finishedImage = [UIImage imageNamed:@"tabbar_normal_bak"];
    UIImage *unfinishedImage = [UIImage imageNamed:@"tabbar_normal_bak"];
    NSArray *tabBarItemImages = @[@"computer", @"finder", @"setting"];
    
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        [item setBackgroundSelectedImage:finishedImage withUnselectedImage:unfinishedImage];
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_icon",
                                                      [tabBarItemImages objectAtIndex:index]]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_icon_n",
                                                        [tabBarItemImages objectAtIndex:index]]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        
        index++;
    }
}

- (void)customizeInterface {
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    
    if ([[[UIDevice currentDevice] systemVersion] integerValue] >= 7.0) {
        [navigationBarAppearance setBackgroundImage:[UIImage imageNamed:@"navbar_tall_bak"]
                                      forBarMetrics:UIBarMetricsDefault];
    } else {
        [navigationBarAppearance setBackgroundImage:[UIImage imageNamed:@"navbar_normal_bak"]
                                      forBarMetrics:UIBarMetricsDefault];
        
        NSDictionary *textAttributes = nil;
        
        if ([[[UIDevice currentDevice] systemVersion] integerValue] >= 7.0) {
            textAttributes = @{
                               NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                               NSForegroundColorAttributeName: [UIColor whiteColor],
                               };
        }/* else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
            textAttributes = @{
                               UITextAttributeFont: [UIFont boldSystemFontOfSize:20],
                               UITextAttributeTextColor: [UIColor blackColor],
                               UITextAttributeTextShadowColor: [UIColor clearColor],
                               UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetZero],
                               };
#endif
        }*/
        
        [navigationBarAppearance setTitleTextAttributes:textAttributes];
    }
}



@end
