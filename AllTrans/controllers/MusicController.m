//
//  MusicController.m
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "MusicController.h"
#import "UICustomization.h"
#import "PlayVideoViewController.h"
#import "utility.h"
#import <AVFoundation/AVFoundation.h>


@implementation MusicController
@synthesize ctlPanel;
@synthesize audioPlayer;
@synthesize title;
@synthesize time;
@synthesize btnPlay;
@synthesize btnopenIn;
@synthesize sliderBar;



- (void)tableView:(UITableView *)tableViewPara didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    long row = indexPath.row;
    if ([self.listData count] > row) {
        NSString * dir = [self.listData objectAtIndex:row];
        
        bool isSupport = true;
        NSString * extension = [[dir pathExtension] lowercaseString];
        if (![extension isEqualToString:@"mp3"]&& ![extension isEqualToString:@"wav"]&& ![extension isEqualToString:@"m4r"]&& ![extension isEqualToString:@"aac"])
            isSupport = false;
        
        if (isSupport) {
            [self.audioPlayer pause];
//            bool isSelf = iCurrentIdex == indexPath.row;
            iCurrentIdex = indexPath.row;
            [self initMusic];
            
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            if (lastcell) {
                for (id subview in lastcell.contentView.subviews) {
                    UIView* uiview = (UIView*)subview;
                    if (uiview.tag == 2)
                    {
                        UIImageView *leftImage = (UIImageView *)subview;
                        leftImage.image = rowImage;
                    }
                    if (uiview.tag == 1) {
                        UILabel *leftLable = (UILabel *)subview;
                        leftLable.textColor = [UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1];
                    }
                }
            }

            for (id subview in cell.contentView.subviews) {
                UIView* uiview = (UIView*)subview;
                if (uiview.tag == 2)
                {
                    UIImageView *leftImage = (UIImageView *)subview;
                    leftImage.image = [UIImage imageNamed:@"music_playing_icon.png"];
                }
                if (uiview.tag == 1) {
                    UILabel *leftLable = (UILabel *)subview;
                    leftLable.textColor = [UIColor colorWithRed:0/255.0 green:122/255.0 blue:255/255.0 alpha:1];
                }
            }
            lastcell = cell;
            
            [self.audioPlayer play];
            [self.btnPlay setSelected:YES];
        }
        else {
            [self openIn:tableViewPara at:row];
            [tableViewPara deselectRowAtIndexPath:[tableViewPara indexPathForSelectedRow] animated:YES];
        }
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (cheatWhenDelete) {
        cheatWhenDelete = 0;
        long row = [self.listData count]>(iPhone5?9:7)?[self.listData count]:(iPhone5?9:7);
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        return [self.listData count]>(iPhone5?10:8)?[self.listData count]:(iPhone5?10:8);
    }
    else
        return [self.listData count]>(iPhone5?10:8)?[self.listData count]:(iPhone5?10:8);
}


- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (ctlPanel == nil) {
        ctlPanel = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 64)];
        ctlPanel.backgroundColor = [UIColor colorWithRed:236.0/255.f green:236.0/255.f blue:236.0/255.f alpha:0.5];
        ctlPanel.userInteractionEnabled = YES;
        
        int time_y = 8;
        if ([utility DeviceSystemMajorVersion] < 7.0) {
            time_y = 18;
        }
        
        self.time = [[UILabel alloc] initWithFrame:CGRectMake(250, time_y, 150, 12)];
        self.time.backgroundColor = [UIColor clearColor];
        [self.time setFont:[UIFont systemFontOfSize:12]];
        self.time.textColor = [UIColor colorWithRed:43.0/255.f green:43.0/255.f blue:43.0/255.f alpha:1.0];
        [ctlPanel addSubview:self.time];
        
        
        UIImage* imageopenIn = [UIImage imageNamed:@"music-play-openin1.png"];
        UIImage* imageopenIn2 = [UIImage imageNamed:@"music-play-openin2.png"];
//        CGRect frameimg = CGRectMake(155, 22, imageopenIn.size.width/2, imageopenIn.size.height/2);
        self.btnopenIn =[[UIButton alloc] init];
        [self.btnopenIn setBackgroundImage:imageopenIn forState:UIControlStateNormal];
        [self.btnopenIn setBackgroundImage:imageopenIn2 forState:UIControlStateHighlighted];
        [self.btnopenIn addTarget:self action:@selector(openInAtCurrent) forControlEvents:UIControlEventTouchUpInside];
//        self.btnopenIn.tag = TAG_MUSIC_OPENIN;
        [ctlPanel addSubview:self.btnopenIn];
        
#define GAP_BETWEEN_BUTTONS 70
        CGRect frame = CGRectMake(0.0, 0.0, self.tableView.bounds.size.width, 10.0);
        self.sliderBar = [[UISlider alloc] initWithFrame:frame];
        [self.sliderBar addTarget:self action:@selector(seakToPosition:) forControlEvents:UIControlEventValueChanged];
        [self.sliderBar setBackgroundColor:[UIColor clearColor]];
        self.sliderBar.minimumValue = 0.0;
//        sliderBar.maximumValue = 50.0;
        self.sliderBar.continuous = YES;
        self.sliderBar.value = 0.0;
        [ctlPanel addSubview:self.sliderBar];
        
        
        UIImage* imageBack = [UIImage imageNamed:@"music_previous"];
        UIImage* imageBack2 = [UIImage imageNamed:@"music_previous"];
        CGRect frameimg = CGRectMake(self.tableView.bounds.size.width/2-15-GAP_BETWEEN_BUTTONS-imageBack.size.width/2, 15, imageBack.size.width+30, imageBack.size.height+30);
        UIButton *btnBack =[[[UIButton alloc] initWithFrame:frameimg] autorelease];
        [btnBack setImage:imageBack forState:UIControlStateNormal];
        [btnBack setImage:imageBack2 forState:UIControlStateHighlighted];
        [btnBack addTarget:self action:@selector(fastRewind) forControlEvents:UIControlEventTouchUpInside];
//        btnBack.tag = TAG_MUSIC_BACKWARD;
        [ctlPanel addSubview:btnBack];
        
        UIImage* imagePlay = [UIImage imageNamed:@"music_play"];
        UIImage* imagePlay2 = [UIImage imageNamed:@"music_play"];
        UIImage* imagePause = [UIImage imageNamed:@"music_pause"];
        frameimg = CGRectMake(self.tableView.bounds.size.width/2-15-imagePlay.size.width/2, 15, imagePlay.size.width+30, imagePlay.size.height+30);
        self.btnPlay =[[UIButton alloc] initWithFrame:frameimg];
        [self.btnPlay setImage:imagePlay forState:UIControlStateNormal];
        [self.btnPlay setImage:imagePlay2 forState:UIControlStateHighlighted];
        [self.btnPlay setImage:imagePause forState:UIControlStateSelected];
        [self.btnPlay addTarget:self action:@selector(togglePlayPauseTapped) forControlEvents:UIControlEventTouchUpInside];
//        self.btnPlay.tag = TAG_MUSIC_PLAY;
        [ctlPanel addSubview:self.btnPlay];
        
        UIImage* imageNext = [UIImage imageNamed:@"music_next"];
        UIImage* imageNext2 = [UIImage imageNamed:@"music_next"];
        frameimg = CGRectMake(self.tableView.bounds.size.width/2-15+GAP_BETWEEN_BUTTONS-imageBack.size.width/2, 15, imageBack.size.width+30, imageBack.size.height+30);
        UIButton *btnNext =[[[UIButton alloc] initWithFrame:frameimg] autorelease];
        [btnNext setImage:imageNext forState:UIControlStateNormal];
        [btnNext setImage:imageNext2 forState:UIControlStateHighlighted];
        [btnNext addTarget:self action:@selector(fastForward) forControlEvents:UIControlEventTouchUpInside];
//        btnNext.tag = TAG_MUSIC_FORWARD;
        [ctlPanel addSubview:btnNext];
    }

    return ctlPanel;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 68.0f;
}

- (bool)canUse:(UITableView *)tableViewPara at:(long)row
{
    if (row == iCurrentIdex && self.btnPlay.selected) {
        return false;
    }
    return [super canUse:tableViewPara at:row];
}

- (void)tableView:(UITableView *)tableViewPara
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    cheatWhenDelete = 1;
    [super tableView:(UITableView *)tableViewPara commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableViewPara cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [super tableView:(UITableView *)tableViewPara cellForRowAtIndexPath:(NSIndexPath *)indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return  cell;
}


#pragma mark - actions

- (void)openInAtCurrent
{
    [self openIn:[self tableView] at:iCurrentIdex];
}

- (void)togglePlayPauseTapped {
    if(self.btnPlay.selected) {
        [self.audioPlayer pause];
        [self.btnPlay setSelected:NO];
    } else {
        if (iCurrentIdex == -1) {
            NSIndexPath* idx = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView selectRowAtIndexPath:idx animated:NO scrollPosition:UITableViewScrollPositionMiddle];
            [self tableView:self.tableView didSelectRowAtIndexPath:idx];
        }
        else {
            [self.audioPlayer play];
            [self.btnPlay setSelected:YES];
        }
    }
    
}

- (void)seakToPosition:(float) position
{
    [self.audioPlayer seekToTime:CMTimeMakeWithSeconds((int)(self.sliderBar.value) , 1)];
}

- (void)fastForward {
/*    NSTimeInterval forwardtime = (self.audioPlayer.currentTime.value)/self.audioPlayer.currentTime.timescale;
    forwardtime += 5; // forward 5 secs
    [self.audioPlayer seekToTime:CMTimeMake(forwardtime, 1) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];*/
    if ([listData count] > iCurrentIdex && iCurrentIdex >=0) {
//        [self.audioPlayer pause];
        iCurrentIdex = iCurrentIdex+1>=[listData count]?0:iCurrentIdex+1;
//        [self initMusic];
//        [self.audioPlayer play];
//        [self.btnPlay setSelected:YES];
        NSIndexPath* idx = [NSIndexPath indexPathForRow:iCurrentIdex inSection:0];
        [self.tableView selectRowAtIndexPath:idx animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        [self tableView:self.tableView didSelectRowAtIndexPath:idx];
    }
}

- (void)fastRewind {
/*    NSTimeInterval forwardtime = (self.audioPlayer.currentTime.value)/self.audioPlayer.currentTime.timescale;
    forwardtime -= 5; // forward 5 secs
    [self.audioPlayer seekToTime:CMTimeMake(forwardtime, 1) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];*/
    if ([listData count] > iCurrentIdex && iCurrentIdex >=0) {
//        [self.audioPlayer pause];
        iCurrentIdex = iCurrentIdex-1<0?0:iCurrentIdex-1;
//        [self initMusic];
//        [self.audioPlayer play];
//        [self.btnPlay setSelected:YES];
        NSIndexPath* idx = [NSIndexPath indexPathForRow:iCurrentIdex inSection:0];
        [self.tableView selectRowAtIndexPath:idx animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        [self tableView:self.tableView didSelectRowAtIndexPath:idx];
    }
}

- (void) initMusic
{
    if ([listData count] > iCurrentIdex && iCurrentIdex >=0) {
        NSArray *array = [[listData objectAtIndex:iCurrentIdex] componentsSeparatedByString:@"."];
        if ([array count]>0) {

//            UIImage* imageopenIn = [UIImage imageNamed:@"music-play-openin1.png"];
//            [self.btnopenIn setFrame:CGRectMake(163-(150-expectedLabelSize.width), 22, imageopenIn.size.width/2, imageopenIn.size.height/2)];
            
            NSString* pathToFile = [[utility getDocumentSubPath:@"Music"] stringByAppendingPathComponent:[listData objectAtIndex:iCurrentIdex]];
            
            AVPlayerItem * currentItem = [AVPlayerItem playerItemWithURL:[NSURL fileURLWithPath:pathToFile]];
            [self.audioPlayer replaceCurrentItemWithPlayerItem:currentItem];
            
            justInit = true;
            self.time.text = @"00:00/00:00";
        }
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    cheatWhenDelete = 0;
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(toggleEdit:)];
    [UICustomization setNVBar:self.navigationController.navigationBar Item:self.navigationItem Button:barButtonItem IsLeft:false];
    [UICustomization setNavText:NSLocalizedString(@"Music", @"Music") item:self.navigationItem];
    
    self.audioPlayer = [[AVPlayer alloc] init];
    __block MusicController * weakSelf = self;


    //8
    [self.audioPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1, 1)
                                                   queue:NULL
                                              usingBlock:^(CMTime timeElapsed) {
                                                  if(!timeElapsed.value || !weakSelf.audioPlayer.currentItem ||
                                                     weakSelf.audioPlayer.currentItem.duration.value == 0 ||
                                                     weakSelf.audioPlayer.currentItem.duration.timescale == 0) {
                                                      return;
                                                  }
                                                  
                                                  int currentTime = (int)((weakSelf.audioPlayer.currentTime.value)/weakSelf.audioPlayer.currentTime.timescale);
                                                  self.sliderBar.value = currentTime;

                                                  
                                                  int currentMins = (int)(currentTime/60);
                                                  int currentSec  = (int)(currentTime%60);
                                                  
                                                  long long duration = weakSelf.audioPlayer.currentItem.duration.value/weakSelf.audioPlayer.currentItem.duration.timescale;
                                                  int durationMins = (int)(duration/60);
                                                  int durationSec  = (int)(duration%60);
                                                  
                                                  if (justInit) {
                                                      justInit = false;
                                                      [weakSelf.sliderBar setMaximumValue:duration];
                                                  }
                                                  
                                                  NSString * durationLabel =
                                                  [NSString stringWithFormat:@"%02d:%02d/%02d:%02d",currentMins,currentSec,durationMins,durationSec];
                                                  weakSelf.time.text = durationLabel;
  //                                                NSLog(@"%d, %d", currentTime, duration);
                                                  if (currentTime >= (duration-1)) {
                                                      [weakSelf fastForward];
                                                  }
                                              }];
    
    //for music player background play
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    // Set AudioSession
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setDelegate:self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    
    /* Pick any one of them */
    // 1. Overriding the output audio route
    //UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    //AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
    
    // 2. Changing the default output audio route
    UInt32 doChangeDefaultRoute = 1;
    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute);
}


- (void)viewWillAppear:(BOOL)animated
{
//    [self initMusic];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self.audioPlayer release];
}

- (id)init
{
    self = [super initWithName:@"Music" Image:@"music_icon.png"];
    if (self) {
        // Custom initialization
    }
    iCurrentIdex = -1;
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

/*- (void)viewControllerDidFinish:(UIViewController *)controller {
	[self dismissModalViewControllerAnimated:YES];
    [controller release];
}*/

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self fastForward];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags NS_AVAILABLE_IOS(6_0)
{
    [self fastForward];
}



@end
