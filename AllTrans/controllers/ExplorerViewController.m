//
//  ExplorerViewController.m
//  FlyTransfer
//
//  Created by  apple on 13-5-16.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ExplorerViewController.h"
#import "LocalImageRootViewController.h"
#import "UICustomization.h"
#import "utility.h"
#import "DirManager.h"
#import "WTZHTTPConnection.h"

@implementation ExplorerViewController
@synthesize listData;
//@synthesize tableView;


- (id)initWithName:(NSString*)name Image:(NSString*)imageName
{
    self = [super init];
    if (self) {
        // Custom initialization
        title_ = name;
        rowImage = [UIImage imageNamed:imageName];
    }
    return self;
}

/*- (void)viewControllerDidFinish:(UIViewController *)controller {
	[self dismissModalViewControllerAnimated:YES];
}*/


- (IBAction)toggleEdit:(id)sender {
	BOOL bEditing = self.tableView.editing;
    [self.tableView setEditing:!bEditing animated:YES];
	
    if (self.tableView.editing)
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemReply target:self action:@selector(toggleEdit:)];
        [[self navigationItem] setRightBarButtonItem:barButtonItem];
//		[UICustomization setNVBar:self.navigationController.navigationBar Item:[self navigationItem] Target:self action:@selector(toggleEdit:) Image:@"delete-ok.png" IsLeft:NO];
    }
    else
    {
//		[UICustomization setNVBar:self.navigationController.navigationBar Item:[self navigationItem] Target:self action:@selector(toggleEdit:) Image:@"trash-full.png" IsLeft:NO];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(toggleEdit:)];
        [[self navigationItem] setRightBarButtonItem:barButtonItem];
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib
- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(notificationCallBack:) name:title_ object:nil];

//    self.tableView.separatorColor = [UIColor colorWithRed:236/255.f green:236/255.f blue:236/255.f alpha:255.f/255.f];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],UITextAttributeTextColor, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
	
	[self update];
	//need setting notification
	
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    UINavigationBar *navbar = [[self navigationController] navigationBar];
    [navbar setAlpha:1];
}

- (void) refreshPicLibrary
{
    [DirManager loadResources];
}

- (void) notificationCallBack:(NSNotification*) notification
{
	[self performSelectorOnMainThread:@selector(update)
						   withObject:nil
						waitUntilDone:YES];
}


- (void)update
{
	NSString *documentsDir = [utility getDocumentSubPath:title_];
	NSError *error = nil;
	NSMutableArray *fileList = [NSMutableArray arrayWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDir error:&error]];
	NSLog(@"路径==%@,fileList%@",documentsDir,fileList);
	
	if (self.listData == nil) {
		self.listData = fileList;
	}

	if ([self.listData count] != [fileList count]) {
		[self.listData removeAllObjects];
		[self.listData addObjectsFromArray:fileList];
		[self.tableView reloadData];
	}
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        return YES;
    }
    return NO;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait; // etc
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
//	[rowImage release];//need?
	[listData release];
    [super dealloc];
}


#pragma mark -
#pragma mark Table View Data Source Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.listData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableViewPara cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];

	UITableViewCell *cell = [tableViewPara dequeueReusableCellWithIdentifier:title_];

    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:title_] autorelease];
    }
    if ([self.listData count] > row) {
        
        CGRect recImage = CGRectMake(20, 18, 15, 18);
        UIImageView *leftImage = [[UIImageView alloc] initWithFrame:recImage];
        leftImage.image = rowImage;
        leftImage.tag = 2;
        [cell.contentView addSubview:leftImage];
        [leftImage release];
        
        CGRect recValue = CGRectMake(49, 18, 260, 20);
        UILabel *value = [[UILabel alloc] initWithFrame:recValue];
        value.textAlignment = NSTextAlignmentLeft;
        value.textColor = [UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1];
        value.font=[UIFont fontWithName:@"Arial" size:18];
        value.backgroundColor = [UIColor clearColor];
        value.tag = 1;
        
        [cell.contentView addSubview:value];
        [value release];
        
        UILabel* titleLab = (UILabel*)[cell.contentView viewWithTag:1];
        titleLab.text = [listData objectAtIndex:row];
    }
	
	return cell;
}


- (BOOL)tableView:(UITableView *)tableViewParm canEditRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return [self canUse:tableViewParm at:[indexPath row]];
}

#pragma mark -
#pragma mark Table View Data Source Methods
- (void)tableView:(UITableView *)tableViewPara
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
	if ([self.listData count] > row) {
        NSString *fileDir = [utility getDocumentSubPath:title_];
        
        NSFileManager *defaultManager = [NSFileManager defaultManager];
        NSString * strDir = [fileDir stringByAppendingPathComponent:[self.listData objectAtIndex:row]];
        
        NSLog(@"delete:%@", strDir);
        [defaultManager removeItemAtPath:strDir error:nil];
        
        [self.listData removeObjectAtIndex:row];
        
        [tableViewPara deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (bool)canUse:(UITableView *)tableViewPara at:(long)row
{
    if ([listData count] > row && row >=0) {
        NSString* content = [listData objectAtIndex:row];
        if ([WTZHTTPConnection getUploading] != nil && [content isEqualToString:[WTZHTTPConnection getUploading]]) {
            [tableViewPara deselectRowAtIndexPath:[tableViewPara indexPathForSelectedRow] animated:YES];
            return false;
        }
        return true;
    }
    return false;
}

- (void)openIn:(UITableView *)tableViewPara at:(long)row
{
    if ([listData count] > row && row >=0) {
        UIDocumentInteractionController * controller;

        NSString* pathToFile = [[utility getDocumentSubPath:title_] stringByAppendingPathComponent:[self.listData objectAtIndex:row]];
        NSURL* url = [NSURL fileURLWithPath:pathToFile];

        controller = [[UIDocumentInteractionController interactionControllerWithURL:url] retain];

        CGRect bounds = CGRectMake (0, 0, 300.0, 300.0);
        [controller  presentOptionsMenuFromRect:bounds  inView: self.view animated: YES];
    }
}
@end

