//
//  FinderViewController.m
//  AllTrans
//
//  Created by Holmes on 1/4/14.
//  Copyright (c) 2014 Holmes. All rights reserved.
//

#import "FinderViewController.h"
#import "DirManager.h"
#import "PhotoController.h"
#import "VideoController.h"
#import "MusicController.h"
#import "DocumentController.h"
#import "UICustomization.h"

@interface FinderViewController ()

#define TAG_NEW_STAIN 3
#define TAG_LEFT_IMAGE 2
#define TAG_TEXT 1

@end

@implementation FinderViewController
@synthesize listData;
@synthesize photoController;
@synthesize musicController;
@synthesize videoController;
@synthesize documentController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if (listData == nil) {
        NSString* picture = NSLocalizedString(@"Picture", @"Picture");
        NSString* video = NSLocalizedString(@"Video", @"Video");
        NSString* music = NSLocalizedString(@"Music", @"Music");
        NSString* other = NSLocalizedString(@"Other", @"Other");
        rowImage1 = [UIImage imageNamed:@"photo_icon"];
        rowImage2 = [UIImage imageNamed:@"video_icon"];
        rowImage3 = [UIImage imageNamed:@"music_icon"];
        rowImage4 = [UIImage imageNamed:@"other_icon"];

        listData = [[NSArray alloc] initWithObjects:picture, video, music, other, nil];
    }
    
//    [self update];
    [UICustomization setNavText:NSLocalizedString(@"finder", @"finder") item:self.navigationItem];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(notificationCallBack4Tab:) name:@"4tab" object:nil];

//    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];


//    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
//    [self navigationItem].titleTextAttributes[] = ;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self update];
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.listData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FinderCell";
    
    long row = [indexPath row];
    
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    if ([self.listData count] > row) {
        
        CGRect recImage = CGRectMake(20, 15, 15, 18);
        UIImageView *leftImage = [[[UIImageView alloc] initWithFrame:recImage] autorelease];
        switch (row) {
            case 0:
                leftImage.image = rowImage1;
                break;
            case 1:
                leftImage.image = rowImage2;
                break;
            case 2:
                leftImage.image = rowImage3;
                break;
            case 3:
                leftImage.image = rowImage4;
                break;
            default:
                break;
        }
        
        leftImage.tag = TAG_LEFT_IMAGE;
        [cell.contentView addSubview:leftImage];
        
        NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
        NSNumber* objnum = [uploadedMap objectForKey:[NSNumber numberWithLong:row]];
        if (objnum != nil) {
            recImage = CGRectMake(260, 17, 10, 10);
            UIImageView *newImage = [[[UIImageView alloc] initWithFrame:recImage] autorelease];
            newImage.image = [UIImage imageNamed:@"new_icon"];
            newImage.tag = TAG_NEW_STAIN;
            [cell.contentView addSubview:newImage];
        }

        
        CGRect recValue = CGRectMake(49, 15, 260, 20);
        UILabel *value = [[UILabel alloc] initWithFrame:recValue];
        value.textAlignment = NSTextAlignmentLeft;
        value.textColor = [UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1];
        value.font=[UIFont fontWithName:@"Arial" size:18];
        value.backgroundColor = [UIColor clearColor];
        value.tag = TAG_TEXT;
        
        [cell.contentView addSubview:value];
        [value release];
        
        UILabel* titleLab = (UILabel*)[cell.contentView viewWithTag:1];
        titleLab.text = [listData objectAtIndex:row];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    
    return cell;
}

- (void) notificationCallBack4Tab:(NSNotification*) notification
{
	[self performSelectorOnMainThread:@selector(update)
						   withObject:nil
						waitUntilDone:YES];
}

- (void) update
{
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap == nil) {
        return;
    }
    for (int i = 0; i < 4; i++) {
        //        NSLog(@"Key:%d,Value:%@",i,[uploadedMap objectForKey:[NSNumber numberWithInt:i]]);
        NSNumber* objnum = [uploadedMap objectForKey:[NSNumber numberWithInt:i]];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        UITableViewCell * cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        if (objnum != nil && ![objnum isEqualToNumber:[NSNumber numberWithInt:0]]) {
            bool isFound = false;
            for (id subview in cell.contentView.subviews) {
                UIView* uiview = (UIView*)subview;
                if (uiview.tag == TAG_NEW_STAIN)
                {
                    isFound = true;
                }
            }
            if (!isFound) {
                CGRect recImage = CGRectMake(260, 17, 10, 10);
                UIImageView *newImage = [[[UIImageView alloc] initWithFrame:recImage] autorelease];
                newImage.image = [UIImage imageNamed:@"new_icon"];
                newImage.tag = TAG_NEW_STAIN;
                [cell.contentView addSubview:newImage];
            }
        }
        else{
            for (id subview in cell.contentView.subviews) {
                UIView* uiview = (UIView*)subview;
                if (uiview.tag == TAG_NEW_STAIN)
                {
                    [uiview removeFromSuperview];
                }
            }
        }
    }
}


- (void)tableView:(UITableView *)tableViewPara didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    switch (row) {
        case 0:
            [self showSubViewPhoto];
            break;
        case 1:
            [self showSubViewVideo];
            break;
        case 2:
            [self showSubViewMusic];
            break;
        case 3:
            [self showSubViewOther];
            break;
        default:
            break;
    }
	
	[tableViewPara deselectRowAtIndexPath:[tableViewPara indexPathForSelectedRow] animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */


- (void)showSubViewOther {

	if (documentController == nil) {
        documentController = [[DocumentController alloc]init];
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:3]];
    }
    [[self navigationController] pushViewController:documentController animated:YES];
}

- (void)showSubViewMusic {
	if (musicController == nil) {
        musicController = [[MusicController alloc] init];
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:2]];
    }
    [[self navigationController] pushViewController:musicController animated:YES];
}

- (void)showSubViewVideo {

	if (videoController == nil) {
        videoController = [[VideoController alloc] init];
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:1]];
    }
    [[self navigationController] pushViewController:videoController animated:YES];
}

- (void)showSubViewPhoto {
	if (photoController == nil) {
        photoController = [[PhotoController alloc] init];
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:0]];
    }
	
    [[self navigationController] pushViewController:photoController animated:YES];
}

@end
