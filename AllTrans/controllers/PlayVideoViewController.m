//
//  PlayVideoViewController.m
//  FlyTransfer
//
//  Created by apple on 13-8-31.
//  Copyright (c) 2013年 Calibri. All rights reserved.
//

#import "PlayVideoViewController.h"
#import "MediaPlayer/MPMoviePlayerViewController.h"
#import "MediaPlayer/MPMoviePlayerController.h"

#import "utility.h"

@implementation PlayVideoViewController
@synthesize _index;
@synthesize playerContr;

-(PlayVideoViewController*) initWithIndex:(long) index
{
    _index = index;
    return self;
}

- (void)viewDidLoad {
    if (playerContr == nil) {
        playerContr = [MPMoviePlayerViewController alloc];

    }
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSString* documentsDir = [utility getDocumentSubPath:@"Video"];
    NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDir error:nil];
    
    NSString* pathToFile = [documentsDir stringByAppendingPathComponent:[fileList objectAtIndex:_index]];
    
    NSURL* url = [NSURL fileURLWithPath:pathToFile];

    playerContr = [playerContr initWithContentURL:url];
    if (playerContr == nil) {
        return;
    }
    
    
//    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(handle_PlaybackStateChanged:) name:MPMoviePlayerNowPlayingMovieDidChangeNotification object: player];
        
//    player.controlStyle = MPMovieControlModeVolumeOnly;
    [playerContr.view setFrame:self.view.bounds];
//    player.initialPlaybackTime = -1;
    [self.view addSubview:playerContr.view];
    //---play movie---
    MPMoviePlayerController *playee = [playerContr moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:playee];

//    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector (handle_PlaybackStateChanged:) name:MPMoviePlayerPlaybackStateDidChangeNotification object: playee];
    [playee play];
    
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        return YES;
    }

    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape; // etc
}

- (void) exitPlay:(MPMoviePlayerController*) oldplayer
{
    NSLog(@"exit");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:oldplayer];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:oldplayer];
    [playerContr removeFromParentViewController];
    [playerContr autorelease];
    
    [self done:nil];
    return;
}

- (void) movieFinishedCallback:(NSNotification*) aNotification {
    MPMoviePlayerController *oldplayer = [aNotification object];

    [self exitPlay:oldplayer];
}

- (void)dealloc {
//	[playerContr release];
    [super dealloc];
}

@end