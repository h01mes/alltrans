//
//  VideoController.h
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExplorerViewController.h"
#import "NavigationalDelegate.h"

@interface VideoController : ExplorerViewController<NavigationalDelegate>


@end
