//
//  OtherController.h
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExplorerViewController.h"
#import "ReaderViewController.h"
@class ReaderViewController;

@interface DocumentController : ExplorerViewController<ReaderViewControllerDelegate>

- (void)dismissReaderViewController:(ReaderViewController *)viewController;


@end
