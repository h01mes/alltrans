//
//  MusicController.h
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExplorerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

//@class AVPlayer;
@interface MusicController : ExplorerViewController <AVAudioPlayerDelegate>
{
    UIView *ctlPanel;
    AVPlayer *audioPlayer;
//    UILabel *title;
    UISlider *sliderBar;
    UILabel *time;
    UIButton *btnPlay;
    UIButton *btnopenIn;
    long iCurrentIdex;
    bool justInit;
//    bool firstPlay;
    int cheatWhenDelete;
    UITableViewCell* lastcell;
}

@property (nonatomic, retain) UIView *ctlPanel;
@property (retain, nonatomic) AVPlayer *audioPlayer;
@property (retain, nonatomic) UILabel *time;
//@property (retain, nonatomic) UILabel *title;
@property (retain, nonatomic) UISlider *sliderBar;
@property (retain, nonatomic) UIButton *btnPlay;

@property (retain, nonatomic) UIButton *btnopenIn;

- (void)openInAtCurrent;
- (void)seakToPosition:(float) position;




@end
