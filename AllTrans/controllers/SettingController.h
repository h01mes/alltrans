//
//  AboutController.h
//  flytransfer
//
//  Created by apple on 13-10-26.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingController : UITableViewController
<UITableViewDelegate, UITableViewDataSource> {
    NSArray *listData;
//    IBOutlet UITableView* m_tableView;
}

@property (nonatomic, retain) NSArray *listData;
//@property (nonatomic, retain) IBOutlet UITableView* m_tableView;

- (UITableViewCell *)tableView:(UITableView *)tableViewPara cellForRowAtIndexPath:(NSIndexPath *)indexPath;
@end
