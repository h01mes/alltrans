//
//  ViewController.h
//  flytransfer
//
//  Created by apple on 13-10-24.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationalDelegate.h"

@class ExplorerViewController;
@class VideoController;
@class MusicController;
@class OtherController;
@class PhotoController;

@class AboutController;
@class UINavigationControllerNoRotate;
@interface ViewController : UIViewController <NavigationalDelegate>
{
    IBOutlet UIImageView* imgBackGround;
    IBOutlet UINavigationBar* viewNavigationBar;
    
    IBOutlet UILabel* labelLine1;
    IBOutlet UILabel* labelLine2;
    IBOutlet UILabel* labelLine3;

    AboutController *aboutController;
    
    OtherController *otherController;
    UINavigationController* navigationOther;
    
    MusicController *musicController;
    UINavigationControllerNoRotate* navigationMusic;

    VideoController *videoController;
    UINavigationController* navigationVideo;

    PhotoController *photoController;
    UINavigationControllerNoRotate* navigationPhoto;
    
    IBOutlet UIButton *but1;
    IBOutlet UIButton *but2;
    IBOutlet UIButton *but3;
    IBOutlet UIButton *but4;
    
    IBOutlet UILabel* lab1;
    IBOutlet UILabel* lab2;
    IBOutlet UILabel* lab3;
    IBOutlet UILabel* lab4;
    
    IBOutlet UIImageView* imageTrans;
    IBOutlet UIButton* butAbout;
}

@property (nonatomic, retain) IBOutlet UIImageView* imgBackGround;
@property (nonatomic, retain) UINavigationBar* viewNavigationBar;
@property (nonatomic, retain) UILabel* labelLine1;
@property (nonatomic, retain) UILabel* labelLine2;
@property (nonatomic, retain) UILabel* labelLine3;

@property (nonatomic, retain) IBOutlet UIButton *but1;
@property (nonatomic, retain) IBOutlet UIButton *but2;
@property (nonatomic, retain) IBOutlet UIButton *but3;
@property (nonatomic, retain) IBOutlet UIButton *but4;

@property (nonatomic, retain) IBOutlet UILabel *lab1;
@property (nonatomic, retain) IBOutlet UILabel *lab2;
@property (nonatomic, retain) IBOutlet UILabel *lab3;
@property (nonatomic, retain) IBOutlet UILabel *lab4;

@property (nonatomic, retain) IBOutlet UIImageView* imageTrans;
@property (nonatomic, retain) IBOutlet UIButton* butAbout;

@property (nonatomic, retain) OtherController *otherController;
@property (nonatomic, retain) AboutController *aboutController;
@property (nonatomic, retain) MusicController *musicController;
@property (nonatomic, retain) VideoController *videoController;
@property (nonatomic, retain) PhotoController *photoController;
@property (nonatomic, retain) UINavigationController* navigationOther;
@property (nonatomic, retain) UINavigationControllerNoRotate* navigationMusic;
@property (nonatomic, retain) UINavigationController* navigationVideo;
@property (nonatomic, retain) UINavigationControllerNoRotate* navigationPhoto;

- (void) mainRefresh;





@end
