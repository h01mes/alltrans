//
//  ToPCViewController.m
//  AllTrans
//
//  Created by Holmes on 1/3/14.
//  Copyright (c) 2014 Holmes. All rights reserved.
//

#import "ToPCViewController.h"
#import "utility.h"
#import "Reachability.h"
#import "UICustomization.h"
#import "DirManager.h"

@interface ToPCViewController ()

@end

@implementation ToPCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self mainRefresh];
    
    [UICustomization setNavText:@"AllTrans" item:self.navigationItem];
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(mainRefresh)];
    [UICustomization setNVBar:self.navigationController.navigationBar Item:self.navigationItem Button:refreshButton IsLeft:false];

}

- (void) refreshAll
{
    [DirManager loadResources];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Music" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Video" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Other" object:nil];
    [self mainRefresh];
}

- (void) mainRefresh
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    UILabel *labelLine1, *labelLine2, *labelLine3;
    
    // Set font and calculate used space
    UIFont *textFont = [UIFont fontWithName:@"Helvetica" size:20];
    
    for (id subview in self.view.subviews) {
        UIView *subView = (UIView *)subview;
        if (subView.tag >= 1 && subView.tag <= 5)
        {
            [subView removeFromSuperview];
//            [subView release];
        }
    }
    
    NSString* tmp;
    int originy = 0;

    if(status == NotReachable || status == ReachableViaWWAN)
    {
        //No internet
        if (status == NotReachable) {
            tmp = NSLocalizedString(@"mainline4", @"No Network");
        }else
            tmp = NSLocalizedString(@"mainline5", @"Current network is 3G");
        
        CGSize textStringSize = [tmp sizeWithFont:textFont constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
        // Position of the text
        if (!iPhone5)
            originy = 150;
        else
            originy = 200;
        labelLine1 = [[[UILabel alloc] initWithFrame:CGRectMake(160-(textStringSize.width/2), originy-(textStringSize.height/2), textStringSize.width,textStringSize.height)] autorelease];
        labelLine1.text = tmp;
        // Set text attributes
        labelLine1.textColor = [UIColor colorWithRed:228/255.f green:0/255.f blue:27/255.f alpha:1.f];
        labelLine1.backgroundColor = [UIColor clearColor];
        labelLine1.font = textFont;
        
        tmp = NSLocalizedString(@"mainline3", @"Please connect to a WiFi first");
        textStringSize = [tmp sizeWithFont:textFont constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
        
        if (!iPhone5)
            originy = 180;
        else
            originy = 230;
        labelLine2 = [[[UILabel alloc] initWithFrame:CGRectMake(160-(textStringSize.width/2), originy-(textStringSize.height/2), textStringSize.width,textStringSize.height)] autorelease];
        labelLine2.text = tmp;
        labelLine2.textColor = [UIColor colorWithRed:228/255.f green:0/255.f blue:27/255.f alpha:1.f];
        labelLine2.backgroundColor = [UIColor clearColor];
        labelLine2.font = textFont;
        
        labelLine1.tag = 1;
        labelLine2.tag = 2;
        [self.view addSubview:labelLine1];
        [self.view addSubview:labelLine2];
    }
    else if (status == ReachableViaWiFi)
    {
        //WiFi
        tmp = NSLocalizedString(@"mainline1", @"Enter this address into");
        CGSize textStringSize = [tmp sizeWithFont:textFont constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
        // Position of the text
        if (!iPhone5)
            originy = 150;
        else
            originy = 200;
        labelLine1 = [[[UILabel alloc] initWithFrame:CGRectMake(160-(textStringSize.width/2), originy-(textStringSize.height/2), textStringSize.width,textStringSize.height)] autorelease];
        labelLine1.text = tmp;
        // Set text attributes
        labelLine1.textColor = [UIColor colorWithRed:97/255.f green:148/255.f blue:188/255.f alpha:1.f];
        labelLine1.backgroundColor = [UIColor clearColor];
        labelLine1.font = textFont;
        
        tmp = NSLocalizedString(@"mainline2", @"the browser on you computer");
        textStringSize = [tmp sizeWithFont:textFont constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
        // Position of the text
        if (!iPhone5)
            originy = 180;
        else
            originy = 230;
        labelLine2 = [[[UILabel alloc] initWithFrame:CGRectMake(160-(textStringSize.width/2), originy-(textStringSize.height/2), textStringSize.width,textStringSize.height)] autorelease];
        labelLine2.text = tmp;
        // Set text attributes
        labelLine2.textColor = [UIColor colorWithRed:97/255.f green:148/255.f blue:188/255.f alpha:1.f];
        labelLine2.backgroundColor = [UIColor clearColor];
        labelLine2.font = textFont;
        
        textFont = [UIFont fontWithName:@"Helvetica" size:17];
        tmp = [utility getIPAddress];
        tmp = [tmp stringByAppendingString:@":8080"];    // Set text attributes
        textStringSize = [tmp sizeWithFont:textFont constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
        if (!iPhone5)
            originy = 225;
        else
            originy = 275;
        labelLine3 = [[[UILabel alloc] initWithFrame:CGRectMake(160-(textStringSize.width/2), originy-(textStringSize.height/2), textStringSize.width,textStringSize.height)] autorelease];
        labelLine3.text = tmp;
        labelLine3.textColor = [UIColor colorWithRed:228/255.f green:0/255.f blue:27/255.f alpha:1.f];
        labelLine3.backgroundColor = [UIColor clearColor];
        labelLine3.font = textFont;
        
        
        UIImage* imageDemo = [UIImage imageNamed:@"home-demo"];
        NSLog(@"%f, %f", imageDemo.size.width, imageDemo.size.height);
        if (!iPhone5)
            originy = 260;
        else
            originy = 310;
        CGRect frameimg = CGRectMake(20, originy, 280, 43);
        UIImageView* imageViewDemo = [[[UIImageView alloc]initWithImage:imageDemo] autorelease];
        [imageViewDemo setFrame:frameimg];
        
        UIImage* imageDemo1 = [UIImage imageNamed:@"home-demo1"];
        NSLog(@"%f, %f", imageDemo1.size.width, imageDemo1.size.height);
        if (!iPhone5)
            originy = 25;
        else
            originy = 50;
        frameimg = CGRectMake(50, originy, imageDemo1.size.width, imageDemo1.size.height);
        UIImageView* imageViewDemo1 = [[[UIImageView alloc]initWithImage:imageDemo1] autorelease];
        [imageViewDemo1 setFrame:frameimg];
        
        imageViewDemo.tag = 4;
        imageViewDemo1.tag = 5;
        [self.view addSubview:imageViewDemo];
        [self.view addSubview:imageViewDemo1];

        
        // Display text
        labelLine1.tag = 1;
        labelLine2.tag = 2;
        labelLine3.tag = 3;

        [self.view addSubview:labelLine1];
        [self.view addSubview:labelLine2];
        [self.view addSubview:labelLine3];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
