//
//  PhotoController.h
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExplorerViewController.h"

typedef enum
{
    PHOTO_VIEW_STATUS_NORMAL = 0,
    PHOTO_VIEW_STATUS_UPLOADING = 1,
    PHOTO_VIEW_STATUS_SYNC = 2,
} PHOTO_VIEW_STATUS;

@interface PhotoController : ExplorerViewController {
    PHOTO_VIEW_STATUS photoStatus;
    double progressValue;
}

@property (strong, nonatomic) UIActivityIndicatorView* spiner;
//@property (strong, nonatomic) UIProgressView* progress;
@property (strong, nonatomic) UIView* overlayView;
@property (strong, nonatomic) UILabel* lableUploading;
@property (strong, nonatomic) UILabel* lableSyncing;


@end
