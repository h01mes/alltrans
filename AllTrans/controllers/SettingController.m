//
//  AboutController.m
//  flytransfer
//
//  Created by apple on 13-10-26.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "SettingController.h"
#import "UICustomization.h"
#import "utility.h"

@implementation SettingController
@synthesize listData;
//@synthesize m_tableView;

/*- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}*/

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.listData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableViewPara cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableViewPara dequeueReusableCellWithIdentifier:@"about"];
    
	NSUInteger row = [indexPath row];
    NSDictionary *rowData = (NSDictionary *)[self.listData objectAtIndex:row];
    
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"about"] autorelease];
        
        CGRect recTitle = CGRectMake(10, 15, 80, 18);
        UILabel *title = [[UILabel alloc] initWithFrame:recTitle];
        title.textAlignment = NSTextAlignmentLeft;
        title.font=[UIFont fontWithName:@"Helvetica-Bold" size:16];
        title.backgroundColor = [UIColor clearColor];
        title.tag = 1;
        title.text = NSLocalizedString([rowData objectForKey:@"title"], @"");
        [cell.contentView addSubview:title];
        [title release];
        
        CGSize textStringSize = [NSLocalizedString([rowData objectForKey:@"value"], @"") sizeWithFont:[UIFont fontWithName:@"Helvetica" size:16] constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
        CGRect recValue = CGRectMake(290-textStringSize.width, 15, 260, 18);
        UILabel *value = [[UILabel alloc] initWithFrame:recValue];
        value.textAlignment = NSTextAlignmentLeft;
        value.textColor = [UIColor colorWithRed:90/255.0 green:90/255.0 blue:90/255.0 alpha:1];
        value.font=[UIFont fontWithName:@"Helvetica" size:16];
        value.backgroundColor = [UIColor clearColor];
        value.tag = 2;
        value.text = NSLocalizedString([rowData objectForKey:@"value"], @"");
        [cell.contentView addSubview:value];
        [value release];
	}

/*    UILabel* titleLab = (UILabel*)[cell.contentView viewWithTag:1];
    titleLab.text = NSLocalizedString([rowData objectForKey:@"title"], @"");
    
    UILabel* valueLab = (UILabel*)[cell.contentView viewWithTag:2];
    valueLab.text = NSLocalizedString([rowData objectForKey:@"value"], @"");*/
    
    //	cell.textLabel.text = [listData objectAtIndex:row];
    //	cell.textLabel.textColor = [UIColor colorWithRed:90/255.0 green:90/255.0 blue:90/255.0 alpha:1];
    //	cell.textLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:16];;
    //	cell.imageView.image = rowImage;
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    long row = indexPath.row;
    if (row == 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"supportrow1value", @"http://www.apprate.net")]];
    }
    else if (row ==1) {
/*        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"support:"];
        NSString* email = @"kolibre_support@hotmail.com";
        NSArray* array = [[NSArray alloc] initWithObjects:email, nil];
        [mailComposer setToRecipients:array];*/
        //        [mailComposer setMessageBody:@"Testing message for the test mail" isHTML:NO];
//        [self presentModalViewController:mailComposer animated:YES];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://alltrans@apprate.net" ]];
    }
    
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
//    navigationBar.topItem.title = NSLocalizedString(@"about", @"about");
    [UICustomization setNavText:NSLocalizedString(@"about", @"about") item:self.navigationItem];
    
    UIView* backView = [[UIView alloc] init];
    [backView setBackgroundColor:[UIColor clearColor]];
    [[self tableView] setBackgroundView:nil];

    NSDictionary *row1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"supportrow1value", @"value", @"supportrow1title", @"title", nil];
    NSDictionary *row2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"supportrow2value", @"value", @"supportrow2title", @"title", nil];
    NSDictionary *row3 = [[NSDictionary alloc] initWithObjectsAndKeys:@"supportrow3value", @"value", @"supportrow3title", @"title", nil];
    
    
    NSArray* array = [[NSArray alloc] initWithObjects:row1, row2, row3, nil];
    self.listData = array;
    
//    mailComposer = [[MFMailComposeViewController alloc]init];
    
    [array release];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    listData = nil;
//    mailComposer = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [listData release];
//    [mailComposer release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
