//
//  FinderViewController.h
//  AllTrans
//
//  Created by Holmes on 1/4/14.
//  Copyright (c) 2014 Holmes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoController;
@class MusicController;
@class DocumentController;
@class PhotoController;

@interface FinderViewController : UITableViewController
{
    NSArray *listData;
    
    DocumentController *documentController;
    
    MusicController *musicController;
    
    VideoController *videoController;
    
    PhotoController *photoController;
    UIImage *rowImage1;
    UIImage *rowImage2;
    UIImage *rowImage3;
    UIImage *rowImage4;
}

@property (nonatomic, retain) NSArray *listData;

@property (nonatomic, retain) DocumentController *documentController;
@property (nonatomic, retain) MusicController *musicController;
@property (nonatomic, retain) VideoController *videoController;
@property (nonatomic, retain) PhotoController *photoController;

@end
