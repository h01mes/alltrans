//
//  ViewController.m
//  flytransfer
//
//  Created by apple on 13-10-24.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//
#import "DirManager.h"
#import "ViewController.h"
#import "utility.h"
#import "AboutController.h"
#import "DocumentController.h"
#import "MusicController.h"
#import "PhotoController.h"
#import "VideoController.h"
#import "UICustomization.h"
#import "UINavigationControllerNoRotate.h"

@implementation ViewController
@synthesize imgBackGround;
@synthesize viewNavigationBar;
@synthesize labelLine1;
@synthesize labelLine2;
@synthesize labelLine3;
@synthesize but1;
@synthesize but2;
@synthesize but3;
@synthesize but4;
@synthesize lab1;
@synthesize lab2;
@synthesize lab3;
@synthesize lab4;

@synthesize otherController;
@synthesize aboutController;
@synthesize musicController;
@synthesize videoController;
@synthesize photoController;
@synthesize navigationOther;
@synthesize navigationMusic;
@synthesize navigationVideo;
@synthesize navigationPhoto;

@synthesize butAbout;
@synthesize imageTrans;


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    aboutController = nil;
    otherController = nil;
    musicController = nil;
    navigationOther = nil;
    navigationVideo = nil;
    navigationMusic = nil;
    navigationPhoto = nil;

    self.view.backgroundColor = [UIColor colorWithRed:92/255.f green:209/255.f blue:205/255.f alpha:255.f/255.f];
    [imgBackGround setBackgroundColor:[UIColor colorWithRed:41/255.f green:194/255.f blue:189/255.f alpha:255/255.f]];
    [UICustomization setNVBar:viewNavigationBar Item:viewNavigationBar.topItem Target:self action:@selector(mainRefresh) Image:@"refresh.png" IsLeft:NO];

    labelLine1.textColor = [UIColor colorWithRed:148/255.f green:242/255.f blue:239/255.f alpha:255.f/255.f];
    labelLine2.textColor = [UIColor colorWithRed:148/255.f green:242/255.f blue:239/255.f alpha:255.f/255.f];
    labelLine3.textColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:255.f/255.f];
    [labelLine1 setFont:[UIFont fontWithName:@"Arial" size:20]];
    [labelLine2 setFont:[UIFont fontWithName:@"Arial" size:20]];
    [labelLine3 setFont:[UIFont fontWithName:@"Arial" size:20]];
    labelLine1.textAlignment = UITextAlignmentCenter;
    labelLine2.textAlignment = UITextAlignmentCenter;
    labelLine3.textAlignment = UITextAlignmentCenter;
    
    lab1.textColor = [UIColor colorWithRed:41/255.f green:194/255.f blue:189/255.f alpha:255.f/255.f];
    lab2.textColor = [UIColor colorWithRed:41/255.f green:194/255.f blue:189/255.f alpha:255.f/255.f];
    lab3.textColor = [UIColor colorWithRed:41/255.f green:194/255.f blue:189/255.f alpha:255.f/255.f];
    lab4.textColor = [UIColor colorWithRed:41/255.f green:194/255.f blue:189/255.f alpha:255.f/255.f];
    [lab1 setFont:[UIFont fontWithName:@"Arial" size:12]];
    [lab2 setFont:[UIFont fontWithName:@"Arial" size:12]];
    [lab3 setFont:[UIFont fontWithName:@"Arial" size:12]];
    [lab4 setFont:[UIFont fontWithName:@"Arial" size:12]];
    lab1.textAlignment = UITextAlignmentCenter;
    lab2.textAlignment = UITextAlignmentCenter;
    lab3.textAlignment = UITextAlignmentCenter;
    lab4.textAlignment = UITextAlignmentCenter;

    if ([utility DeviceSystemMajorVersion] < 7) {
        //self.view.backgroundColor = [UIColor colorWithRed:212/255.f green:243/255.f blue:242/255.f alpha:255/255.f];
        self.view.backgroundColor = [UIColor colorWithRed:41/255.f green:194/255.f blue:189/255.f alpha:255/255.f];
        CGRect frame = viewNavigationBar.frame;
        int offset = 20;
        frame.origin.y -= offset;
        viewNavigationBar.frame = frame;
        
        frame = imgBackGround.frame;
        frame.origin.y -= offset;
        imgBackGround.frame= frame;
        
        frame = butAbout.frame;
        frame.origin.y -= offset;
        butAbout.frame= frame;
        
        offset = 25;
        frame = labelLine1.frame;
        frame.origin.y -= offset;
        labelLine1.frame= frame;
        
        frame = labelLine2.frame;
        frame.origin.y -= offset;
        labelLine2.frame= frame;
        
        frame = labelLine3.frame;
        frame.origin.y -= offset;
        labelLine3.frame= frame;
        
        if (iPhone5) {
            offset = 20;
        }
        else
            offset = 10;
        
        frame = but1.frame;
        frame.origin.y -= offset;
        but1.frame= frame;
        
        frame = but2.frame;
        frame.origin.y -= offset;
        but2.frame= frame;
        
        frame = but3.frame;
        frame.origin.y -= offset;
        but3.frame= frame;
        
        frame = but4.frame;
        frame.origin.y -= offset;
        but4.frame= frame;
        
        frame = lab1.frame;
        frame.origin.y -= offset;
        lab1.frame= frame;
        
        frame = lab2.frame;
        frame.origin.y -= offset;
        lab2.frame= frame;
        
        frame = lab3.frame;
        frame.origin.y -= offset;
        lab3.frame= frame;
        
        frame = lab4.frame;
        frame.origin.y -= offset;
        lab4.frame= frame;
        
        frame = imageTrans.frame;
        frame.origin.y -= offset;
        imageTrans.frame= frame;
    }
    
    NSLog(@"%f, %f", [[UIScreen mainScreen] currentMode].size.height, [[UIScreen mainScreen] currentMode].size.width);
    
    if (!iPhone5) {
        CGRect buttonFrame = but1.frame;
        buttonFrame.origin.y -= 90;
        but1.frame = buttonFrame;
        
        buttonFrame = but2.frame;
        buttonFrame.origin.y -= 90;
        but2.frame = buttonFrame;
        
        buttonFrame = but3.frame;
        buttonFrame.origin.y -= 90;
        but3.frame = buttonFrame;
        
        buttonFrame = but4.frame;
        buttonFrame.origin.y -= 90;
        but4.frame = buttonFrame;
        
        buttonFrame = lab1.frame;
        buttonFrame.origin.y -= 90;
        lab1.frame = buttonFrame;
        
        buttonFrame = lab2.frame;
        buttonFrame.origin.y -= 90;
        lab2.frame = buttonFrame;
        
        buttonFrame = lab3.frame;
        buttonFrame.origin.y -= 90;
        lab3.frame = buttonFrame;
        
        buttonFrame = lab4.frame;
        buttonFrame.origin.y -= 90;
        lab4.frame = buttonFrame;
    }
    
    lab1.text = NSLocalizedString(@"Picture", @"Picture");
    lab2.text = NSLocalizedString(@"Music", @"Music");
    lab3.text = NSLocalizedString(@"Video", @"Video");
    lab4.text = NSLocalizedString(@"Other", @"Other");

    labelLine1.text = NSLocalizedString(@"mainline1", @"Enter this address into");
    labelLine2.text = NSLocalizedString(@"mainline2", @"the browser on you computer");
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(notificationCallBack4Tab:) name:@"4tab" object:nil];
}

- (void) mainRefresh
{
    NSString* address = [utility getIPAddress];
    if ([address isEqualToString:@"0.0.0.0"])
        labelLine3.text = NSLocalizedString(@"mainline3", @"Please connect to your WiFi first");
    else
        labelLine3.text = [address stringByAppendingString:@":8080"];

    [DirManager loadResources];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Music" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Video" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Other" object:nil];
}

- (void) notificationCallBack4Tab:(NSNotification*) notification
{
	[self performSelectorOnMainThread:@selector(update)
						   withObject:nil
						waitUntilDone:YES];
}

- (void) update
{
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap == nil) {
        return;
    }
    for (int i = 0; i < 4; i++) {
        //        NSLog(@"Key:%d,Value:%@",i,[uploadedMap objectForKey:[NSNumber numberWithInt:i]]);
        NSNumber* objnum = [uploadedMap objectForKey:[NSNumber numberWithInt:i]];
        if (objnum != nil) {
            switch (i) {
                case 0:
                    if (![objnum isEqualToNumber:[NSNumber numberWithInt:0]]) {
                        [but1 setImage:[UIImage imageNamed:@"home-photo2.png"] forState:UIControlStateNormal];
                    }
                    else
                        [but1 setImage:[UIImage imageNamed:@"home-photo0.png"] forState:UIControlStateNormal];
                                        break;
                case 2:
                    if (![objnum isEqualToNumber:[NSNumber numberWithInt:0]]) {
                        [but2 setImage:[UIImage imageNamed:@"home-music2.png"] forState:UIControlStateNormal];
                    }
                    else
                        [but2 setImage:[UIImage imageNamed:@"home-music0.png"] forState:UIControlStateNormal];
                    break;
                case 1:
                    if (![objnum isEqualToNumber:[NSNumber numberWithInt:0]]) {
                        [but3 setImage:[UIImage imageNamed:@"home-video2.png"] forState:UIControlStateNormal];
                    }
                    else
                        [but3 setImage:[UIImage imageNamed:@"home-video0.png"] forState:UIControlStateNormal];
                    break;
                case 3:
                    if (![objnum isEqualToNumber:[NSNumber numberWithInt:0]]) {
                        [but4 setImage:[UIImage imageNamed:@"home-other2.png"] forState:UIControlStateNormal];
                    }
                    else
                        [but4 setImage:[UIImage imageNamed:@"home-other0.png"] forState:UIControlStateNormal];
                    break;
                    
                default:
                    break;
            }
        }
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    NSString* address = [utility getIPAddress];
    if ([address isEqualToString:@"0.0.0.0"])
        labelLine3.text = NSLocalizedString(@"mainline3", @"Please connect to your WiFi first");
    else
        labelLine3.text = [address stringByAppendingString:@":8080"];

    [self update];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait; // etc
}

- (IBAction)showSubViewAbout:(id)sender {
    if (aboutController == nil) {
        aboutController = [[AboutController alloc] initWithNibName:@"AboutController" bundle:nil];
    }
//	aboutController.nav_delegate = self;
	
	aboutController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	[self presentModalViewController:aboutController animated:YES];
}

- (IBAction)showSubViewOther:(id)sender {
    if (navigationOther == nil) {
        navigationOther = [UINavigationController alloc];
    }
	if (otherController == nil) {
        otherController = [[OtherController alloc]init];
        [navigationOther initWithRootViewController:otherController];
//        otherController.nav_delegate = self;
        navigationOther.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:3]];
    }
    
	[self presentModalViewController:navigationOther animated:YES];
}

- (IBAction)showSubViewMusic:(id)sender {
    if (navigationMusic == nil) {
        navigationMusic = [UINavigationControllerNoRotate alloc];
    }
	if (musicController == nil) {
        musicController = [[MusicController alloc] init];
        [navigationMusic initWithRootViewController:musicController];
//        musicController.nav_delegate = self;
        navigationMusic.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:2]];
    }
    
	[self presentModalViewController:navigationMusic animated:YES];
}

- (IBAction)showSubViewVideo:(id)sender {
    if (navigationVideo == nil) {
        navigationVideo = [UINavigationController alloc];
    }
	if (videoController == nil) {
        videoController = [[VideoController alloc] init];
        [navigationVideo initWithRootViewController:videoController];
//        videoController.nav_delegate = self;
        navigationVideo.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:1]];
    }
    
	[self presentModalViewController:navigationVideo animated:YES];
}

- (IBAction)showSubViewPhoto:(id)sender {
    if (navigationPhoto == nil) {
        navigationPhoto = [UINavigationControllerNoRotate alloc];
    }
	if (photoController == nil) {
        photoController = [[PhotoController alloc] init];
        [navigationPhoto initWithRootViewController:photoController];
//        photoController.nav_delegate = self;
        navigationPhoto.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];
    if (uploadedMap != nil) {
        [uploadedMap setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:0]];
    }
	
	[self presentModalViewController:navigationPhoto animated:YES];
}

- (void)viewControllerDidFinish:(UIViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
}

@end
