//
//  VideoController.m
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "VideoController.h"
#import "UICustomization.h"
#import "PlayVideoViewController.h"

@implementation VideoController

- (id)init
{
    self = [super initWithName:@"Video" Image:@"video_icon"];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewControllerDidFinish:(UIViewController *)controller {
	[self dismissViewControllerAnimated:YES completion:nil];
    [controller release];
}

- (void)tableView:(UITableView *)tableViewPara didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    long row = indexPath.row;
    NSString * dir = [self.listData objectAtIndex:row];
    
    bool isSupport = true;
    NSString * extension = [[dir pathExtension] lowercaseString];
    if (![extension isEqualToString:@"mov"] && ![extension isEqualToString:@"mp4"]&& ![extension isEqualToString:@"m4v"])
        isSupport = false;
    
    if (isSupport) {
        PlayVideoViewController *playerController = [[PlayVideoViewController alloc] initWithIndex:row];
        
        NSUserDefaults *userDefaut = [NSUserDefaults standardUserDefaults];
        long nCount = [userDefaut integerForKey:@"VIDEO_REPORT"];
        [userDefaut setInteger:nCount+1 forKey:@"VIDEO_REPORT"];
        
        playerController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        playerController.nav_delegate = self;
        [self presentViewController:playerController animated:YES completion:nil];
    }else {
        [self openIn:tableViewPara at:row];
    }
	
	[tableViewPara deselectRowAtIndexPath:[tableViewPara indexPathForSelectedRow] animated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [UICustomization setNVBar:self.navigationController.navigationBar Item:[self navigationItem] Target:self action:@selector(toggleEdit:) Image:@"trash-full.png" IsLeft:NO];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(toggleEdit:)];
    [UICustomization setNVBar:self.navigationController.navigationBar Item:self.navigationItem Button:barButtonItem IsLeft:false];

    [UICustomization setNavText:NSLocalizedString(@"Video", @"Video") item:self.navigationItem];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
