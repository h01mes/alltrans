//
//  PlayVideoViewController.h
//  FlyTransfer
//
//  Created by apple on 13-8-31.
//  Copyright (c) 2013年 Calibri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NavigationalSubView.h"

@class MPMoviePlayerController;
@class MPMoviePlayerViewController;

@interface PlayVideoViewController : NavigationalSubView
{
    long _index;
    MPMoviePlayerViewController *playerContr;
}

@property (nonatomic, assign) long _index;
@property (nonatomic, retain) MPMoviePlayerViewController *playerContr;


-(PlayVideoViewController*) initWithIndex:(long) index;
- (void) exitPlay:(MPMoviePlayerController*) oldplayer;


@end
