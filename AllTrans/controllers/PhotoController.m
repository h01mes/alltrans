//
//  PhotoController.m
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "PhotoController.h"
#import "UICustomization.h"
#import "WTZHTTPConnection.h"
#import "DirManager.h"
#import "DirInfo.h"
#import "LocalImageRootViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation PhotoController


- (id)init
{
    self = [super initWithName:@"Picture" Image:@"no_photo_icon"];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    [UICustomization setNVBar:self.navigationController.navigationBar Item:self.navigationItem Button:refreshButton IsLeft:false];
    
//    [UICustomization setNVBar:self.navigationController.navigationBar Item:[self navigationItem] Target:[DirManager class] action:@selector(loadResources) Image:@"refresh.png" IsLeft:NO];
    
    self.overlayView = [[UIView alloc] init];
    self.overlayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    self.overlayView.frame = self.tableView.bounds;
    [self.tableView addSubview:self.overlayView];
    
    self.spiner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, -50, 50, 50)];
    self.spiner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [self.spiner setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2)];
    [self.overlayView addSubview:self.spiner];
/*     self.progress = [[UIProgressView alloc] initWithFrame:CGRectMake(50, 120, 220, 50)];
    self.progress.progressViewStyle = UIProgressViewStyleBar;
     [self.overlayView addSubview:self.self.progress];*/
    
    UIFont *textFont = [UIFont fontWithName:@"Helvetica" size:20];

    NSString *tmp = NSLocalizedString(@"uploading", @"uploading");
    CGSize textStringSize = [tmp sizeWithFont:textFont constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
    // Position of the text

    self.lableUploading = [[[UILabel alloc] initWithFrame:CGRectMake(160-(textStringSize.width/2), 120-(textStringSize.height/2), textStringSize.width,textStringSize.height)] autorelease];
    self.lableUploading.text = tmp;
    // Set text attributes
    self.lableUploading.textColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:1.f];
    self.lableUploading.backgroundColor = [UIColor clearColor];
    self.lableUploading.font = textFont;
    self.lableUploading.hidden = YES;
    
    tmp = NSLocalizedString(@"saving to camera roll", @"saving to camera roll");
    textStringSize = [tmp sizeWithFont:textFont constrainedToSize:CGSizeMake(300,50) lineBreakMode:NSLineBreakByTruncatingTail];
    // Position of the text
    
    self.lableSyncing = [[[UILabel alloc] initWithFrame:CGRectMake(160-(textStringSize.width/2), 120-(textStringSize.height/2), textStringSize.width,textStringSize.height)] autorelease];
    self.lableSyncing.text = tmp;
    // Set text attributes
    self.lableSyncing.textColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:1.f];
    self.lableSyncing.backgroundColor = [UIColor clearColor];
    self.lableSyncing.font = textFont;
    self.lableSyncing.hidden = YES;
    
    [self.overlayView addSubview:self.lableUploading];
    [self.overlayView addSubview:self.lableSyncing];
    if (uploadCount) {
        photoStatus = PHOTO_VIEW_STATUS_UPLOADING;
        [self updatePhotoView];
    }
    if ([DirManager isSync]) {
        photoStatus = PHOTO_VIEW_STATUS_SYNC;
        [self updatePhotoView];
    }
    else
    {
        photoStatus = PHOTO_VIEW_STATUS_NORMAL;
        [self updatePhotoView];
    }
    
    [UICustomization setNavText:NSLocalizedString(@"Picture", @"Picture") item:self.navigationItem];
    
    progressValue = 100;
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(uploadNotificationCallBack:) name:@"4photoupload" object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(syncNotificationCallBack:) name:@"4photosync" object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(syncOverNotificationCallBack:) name:@"4photosyncover" object:nil];
//    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(syncOverNotificationCallBack:) name:@"4photouploadprogress" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (uploadCount) {
        photoStatus = PHOTO_VIEW_STATUS_UPLOADING;
        [self updatePhotoView];
    }
    if ([DirManager isSync]) {
        photoStatus = PHOTO_VIEW_STATUS_SYNC;
        [self updatePhotoView];
    }
    else
    {
        photoStatus = PHOTO_VIEW_STATUS_NORMAL;
        [self updatePhotoView];
    }
}

- (void)refresh
{
    if (uploadCount == 0 && ![DirManager isSync])
        [DirManager loadResources];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UITableViewCell *)tableView:(UITableView *)tableViewPara cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    
	UITableViewCell *cell = [tableViewPara dequeueReusableCellWithIdentifier:title_];
    
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:title_] autorelease];
        
        CGRect recImage = CGRectMake(10, 12, 70, 70);
        UIImageView *leftImage = [[UIImageView alloc] initWithFrame:recImage];
        leftImage.tag = 1;
        [cell.contentView addSubview:leftImage];
        [leftImage release];
        
        CGRect recValue = CGRectMake(88, 35, 210, 20);
        UILabel *value = [[UILabel alloc] initWithFrame:recValue];
        value.textAlignment = NSTextAlignmentLeft;
        value.textColor = [UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1];
        value.font=[UIFont fontWithName:@"Arial" size:18];
        value.backgroundColor = [UIColor clearColor];
        value.tag = 2;
        
        [cell.contentView addSubview:value];
        [value release];
	}
    NSString* albumName = [listData objectAtIndex:row];
    UILabel* titleLab = (UILabel*)[cell.contentView viewWithTag:2];
    titleLab.text = albumName;
	
    UIImageView* imageView = (UIImageView*)[cell.contentView viewWithTag:1];
    DirInfo* thisDir = [[DirManager getDirMap] objectForKey:albumName];
    if (thisDir && [thisDir.assets count] > 0)
    {
        ALAsset* thisasset = [thisDir.assets objectAtIndex:0];
        imageView.image = [UIImage imageWithCGImage:[thisasset thumbnail]];
    }
    else
    {
        imageView.image = rowImage;
    }

    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	return cell;
}

- (void)tableView:(UITableView *)tableViewPara didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    long row = indexPath.row;
    NSString * dir = [self.listData objectAtIndex:row];

    LocalImageRootViewController *newController = [[LocalImageRootViewController alloc]initWithDir:dir];
    [[self navigationController] pushViewController:newController
										   animated:YES];
    [newController release];
	
	[tableViewPara deselectRowAtIndexPath:[tableViewPara indexPathForSelectedRow] animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

- (void) uploadProgressNotificationCallBack:(NSNotification*) notification
{
    photoStatus = PHOTO_VIEW_STATUS_UPLOADING;
    progressValue = [(NSNumber*)[notification userInfo] doubleValue];
    [self performSelectorOnMainThread:@selector(updatePhotoView)
						   withObject:nil
						waitUntilDone:YES];
}

- (void) uploadNotificationCallBack:(NSNotification*) notification
{
    photoStatus = PHOTO_VIEW_STATUS_UPLOADING;
    [self performSelectorOnMainThread:@selector(updatePhotoView)
						   withObject:nil
						waitUntilDone:YES];
}

- (void) syncNotificationCallBack:(NSNotification*) notification
{
    photoStatus = PHOTO_VIEW_STATUS_SYNC;
	[self performSelectorOnMainThread:@selector(updatePhotoView)
						   withObject:nil
						waitUntilDone:YES];

    
}

- (void) syncOverNotificationCallBack:(NSNotification*) notification
{
    NSLog(@"upload:%d, sync:%d", uploadCount, [DirManager isSync]);
    if (uploadCount == 0 && ![DirManager isSync]) {
        photoStatus = PHOTO_VIEW_STATUS_NORMAL;
        [self performSelectorOnMainThread:@selector(updatePhotoView)
                               withObject:nil
                            waitUntilDone:YES];
    }
}

-(void) updatePhotoView
{
    NSLog(@"photo status change to%d", photoStatus);
    
    if (PHOTO_VIEW_STATUS_UPLOADING == photoStatus) {
        self.overlayView.hidden = NO;
        self.tableView.alwaysBounceVertical = NO;
        [self.spiner startAnimating];
//        self.progress.progress = progressValue;
        self.lableSyncing.hidden = YES;
        self.lableUploading.hidden = NO;

    }
    else if (PHOTO_VIEW_STATUS_SYNC == photoStatus) {
        self.overlayView.hidden = NO;
        self.tableView.alwaysBounceVertical = NO;
        [self.spiner startAnimating];
//        self.progress.progress = 100;
        self.lableSyncing.hidden = NO;
        self.lableUploading.hidden = YES;
    }
    else if (PHOTO_VIEW_STATUS_NORMAL == photoStatus) {
        self.overlayView.hidden = YES;
        self.tableView.alwaysBounceVertical = YES;
        [self.spiner stopAnimating];

        [NSTimer scheduledTimerWithTimeInterval:0.5 target:DirManager.class selector:@selector(loadResources) userInfo:Nil repeats:false];
//        [DirManager loadResources];
    }
}

@end
