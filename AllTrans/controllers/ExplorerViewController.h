//
//  ExplorerViewController.h
//  FlyTransfer
//
//  Created by  apple on 13-5-16.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ExplorerViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
	NSMutableArray *listData;
//    IBOutlet UITableView *tableView;

	UIImage *rowImage;
	NSString* title_;
}

@property (nonatomic, retain) NSMutableArray *listData;
//@property(nonatomic,retain) IBOutlet UITableView *tableView;

//@property (retain, nonatomic) UIImage *rowImage;

- (void)update;
//- (id)initWithName:(NSString*)name Image:(NSString*)imageName NibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

- (id)initWithName:(NSString*)name Image:(NSString*)imageName;

//- (void)viewControllerDidFinish:(UIViewController *)controller;

- (IBAction)toggleEdit:(id)sender;
- (bool)canUse:(UITableView *)tableViewPara at:(long)row;
- (void)openIn:(UITableView *)tableViewPara at:(long)row;

@end
