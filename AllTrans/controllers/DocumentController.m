//
//  OtherController.m
//  flytransfer
//
//  Created by apple on 13-10-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "DocumentController.h"
#import "UICustomization.h"
#import "utility.h"
#import "ReaderViewController.h"

@implementation DocumentController

- (id)init
{
    self = [super initWithName:@"Other" Image:@"other_icon"];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)tableView:(UITableView *)tableViewPara didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    long row = indexPath.row;
    NSString * dir = [self.listData objectAtIndex:row];

    bool isSupport = true;
    NSString * extension = [[dir pathExtension] lowercaseString];
    if (![extension isEqualToString:@"pdf"] || [utility DeviceSystemMajorVersion] < 6)
        isSupport = false;
    
    NSString* documentsDir = [utility getDocumentSubPath:@"Other"];
    NSString* pathToFile = [documentsDir stringByAppendingPathComponent:[self.listData objectAtIndex:row]];
    
    if (isSupport) {
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:pathToFile password:@""];
        
        if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
        {
            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            NSUserDefaults *userDefaut = [NSUserDefaults standardUserDefaults];
            long nCount = [userDefaut integerForKey:@"PDF_READER_REPORT"];
            [userDefaut setInteger:nCount+1 forKey:@"PDF_READER_REPORT"];
//            [self.navigationController pushViewController:readerViewController animated:YES];
            readerViewController.delegate = self; // Set the ReaderViewController delegate to self

            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [self presentViewController:readerViewController animated:YES completion:NULL];
        }
    }
    else {
        [self openIn:tableViewPara at:row];
    }
	
	[tableViewPara deselectRowAtIndexPath:[tableViewPara indexPathForSelectedRow] animated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [UICustomization setNVBar:self.navigationController.navigationBar Item:[self navigationItem] Target:self action:@selector(toggleEdit:) Image:@"trash-full.png" IsLeft:NO];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(toggleEdit:)];
    [UICustomization setNVBar:self.navigationController.navigationBar Item:self.navigationItem Button:barButtonItem IsLeft:false];
    [UICustomization setNavText:NSLocalizedString(@"Other", @"Other") item:self.navigationItem];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    
	[self dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
