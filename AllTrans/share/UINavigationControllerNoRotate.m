//
//  UINavigationControllerNoRotate.m
//  flytransfer
//
//  Created by Holmes on 12/13/13.
//
//

#import "UINavigationControllerNoRotate.h"

@implementation UINavigationControllerNoRotate
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        return YES;
    }
    return NO;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait; // etc
}
@end
