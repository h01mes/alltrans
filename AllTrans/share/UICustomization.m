//
//  UICustomization.m
//  FlyTransfer
//
//  Created by  apple on 13-7-23.
//  Copyright 2013 Calibri. All rights reserved.
//

#import "UICustomization.h"
#import "utility.h"

#define NAV_BAR_BACK [UIColor colorWithRed:165/255.f green:209/255.f blue:243/255.f alpha:255.f/255.f]
#define NAV_TEXT [UIColor colorWithRed:0/255.f green:122/255.f blue:255/255.f alpha:255.f/255.f]


@implementation UICustomization
+ (void) setBackGround:(UIViewController*)selfController Image:(NSString*)imageName
{
	UIImage *backImage = [UIImage imageNamed:imageName];
	selfController.view.backgroundColor = [UIColor colorWithPatternImage:backImage];
}

+ (void) setNVBar:(UINavigationBar*)selfBar Item:(UINavigationItem*)selfItem Target:(id)target action:(SEL)action Image:(NSString*)imageName IsLeft:(BOOL)isleft
{
	UIImage* image4BackBtn = [UIImage imageNamed:imageName];
	CGRect frameimg = CGRectMake(0, 0, image4BackBtn.size.width, image4BackBtn.size.height);
	
	UIButton *btnDone =[[UIButton alloc] initWithFrame:frameimg];
	[btnDone setBackgroundImage:image4BackBtn forState:UIControlStateNormal];
	[btnDone addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	[btnDone setShowsTouchWhenHighlighted:YES];
	
	UIBarButtonItem *barButtonItem =[[UIBarButtonItem alloc] initWithCustomView:btnDone];

	if (isleft) {
		[selfItem setLeftBarButtonItem:barButtonItem];
        if ([selfItem.leftBarButtonItem respondsToSelector:@selector(setTintColor:)]) {
            [selfItem.leftBarButtonItem setTintColor:NAV_BAR_BACK];
        }
	}
	else {
		[selfItem setRightBarButtonItem:barButtonItem];
        if ([selfItem.rightBarButtonItem respondsToSelector:@selector(setTintColor:)]) {
            [selfItem.leftBarButtonItem setTintColor:NAV_BAR_BACK];
        }
	}
    
    if (![selfBar respondsToSelector:@selector(setBarTintColor:)]) {
        [selfBar setTintColor:NAV_BAR_BACK];
    }
    else
    {
        [selfBar setBarTintColor:NAV_BAR_BACK];
    }
    
    if (selfItem.leftBarButtonItem) {
        [selfItem.leftBarButtonItem setTintColor:NAV_BAR_BACK];
    }
    
    selfBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
	[btnDone release];
	[barButtonItem release];
}

+ (void) setNVBar:(UINavigationBar*)selfBar Item:(UINavigationItem*)selfItem Button:(UIBarButtonItem*)barButtonItem IsLeft:(BOOL)isleft
{
    if (isleft) {
        [selfItem setLeftBarButtonItem:barButtonItem];
        if ([selfItem.leftBarButtonItem respondsToSelector:@selector(setTintColor:)]) {
            [selfItem.leftBarButtonItem setTintColor:NAV_BAR_BACK];
        }
    }
    else
    {
        [selfItem setRightBarButtonItem:barButtonItem];
        if ([selfItem.rightBarButtonItem respondsToSelector:@selector(setTintColor:)]) {
            [selfItem.leftBarButtonItem setTintColor:NAV_BAR_BACK];
        }
    }


    if (![selfBar respondsToSelector:@selector(setBarTintColor:)]) {
        [selfBar setTintColor:NAV_BAR_BACK];
    }
    else
    {
        [selfBar setBarTintColor:NAV_BAR_BACK];
    }
}

+ (void) setTabBar:(UITabBar*)tabBar withColor:(UIColor*)color andWidth:(int)width andHeight:(int)height
{
	CGRect frame = CGRectMake(0.0, 0.0, width, height);
    UIView *v = [[UIView alloc] initWithFrame:frame];
    [v setBackgroundColor:color];
    [v setAlpha:0.5];
    [tabBar insertSubview:v atIndex:0];
    [v release];
}

+ (void) setTabBar:(UITabBar*)tabBar withImage:(NSString*)imageName andWidth:(int)width andHeight:(int)height
{
	CGRect frame = CGRectMake(0, 0, width, height);
	UIView *v = [[UIView alloc] initWithFrame:frame];
	UIImage *i = [UIImage imageNamed:imageName];
	UIColor *c = [[UIColor alloc] initWithPatternImage:i];
	v.backgroundColor = c;
	[c release];
    [tabBar insertSubview:v atIndex:0];
	[v release];
}

+ (void) setNavText:(NSString*) title item:(UINavigationItem*) navigationItem
{
    UILabel *titleView = (UILabel *)navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
//        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        //        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        if ([utility DeviceSystemMajorVersion] >= 7)
        {
            titleView.font = [UIFont boldSystemFontOfSize:18.0];
            titleView.textColor = NAV_TEXT; // Change to desired color
        }
        else
        {
            titleView.font = [UIFont systemFontOfSize:18.0];
            titleView.textColor = [UIColor whiteColor];
        }
        
        navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

@end
