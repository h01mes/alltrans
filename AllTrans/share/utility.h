//
//  utility.h
//  FlyTransfer
//
//  Created by  apple on 13-8-18.
//  Copyright 2013 Calibri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

@interface utility : NSObject {
}

+ (NSString*) getDocumentPath;
+ (NSString*) getDocumentSubPath:(NSString*)subDir;
+ (NSString *)encodeToPercentEscapeString: (NSString *) input;
+ (NSString *)decodeFromPercentEscapeString: (NSString *) input;

+ (void)copyFromAsset:(ALAsset*)asset toFile:(NSString*) filepath;

+ (NSString *)getIPAddress;

+ (void)truncateDir:(NSString*) pathname;
+ (void)truncateResourceAtDir:(NSString*) pathname;
+ (void)copyResourceAtDir:(NSString*) pathname from:(NSString*) resourceDir;

+ (NSUInteger) DeviceSystemMajorVersion;


@end
