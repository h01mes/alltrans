//
//  Statistics.m
//  flytransfer
//
//  Created by Holmes on 11/7/13.
//
//

#import "Statistics.h"
#import "utility.h"
#import "DirManager.h"
#import "DirInfo.h"
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

InstallReportDelegate* insDelegate = nil;
ForegroundReportDelegate* foreDelegate = nil;

@implementation Statistics

+ (void) reportInstall
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* uid = [userDefaults valueForKey:@"userid"];
    
    if (!uid) {
        CFUUIDRef   uuidObj = CFUUIDCreate(nil);
        uid = (NSString*)CFUUIDCreateString(nil, uuidObj);
        CFRelease(uuidObj);
        [userDefaults setObject:uid forKey:@"userid"];
    }
/*    NSString* albums = @"";
    NSMutableDictionary* mapAlbums = [DirManager getDirMap];
    if (mapAlbums != nil) {
        for (NSString* key in mapAlbums) {
            albums = [albums stringByAppendingString:key];
            albums = [albums stringByAppendingString:@":"];
        }
    }
    NSData* dataAlbums = [albums dataUsingEncoding:NSUTF8StringEncoding];
    NSString* resultAlbums = [utility encodeToPercentEscapeString:[dataAlbums base64Encoding]];
    NSLog(@"%@", resultAlbums);
 */
    NSString* resultAlbums = @"";
    NSString* version = NSLocalizedString(@"supportrow3value", @"");
    NSArray *languageArray = [NSLocale preferredLanguages];
    
    

    NSString* lang = @"3";
    if ([languageArray count] > 0) {
        lang = [languageArray objectAtIndex:0];
    }
    if ([lang hasPrefix:@"en"]) {
        lang = @"0";
    }
    else if ([lang isEqualToString:@"ja"])
    {
        lang = @"1";
    }
    else if ([lang hasPrefix:@"zh"])
    {
        lang = @"2";
    }
    
    unsigned long ios = [utility DeviceSystemMajorVersion];
    NSString* resolution = [NSString stringWithFormat:@"%d*%d",(int)[[UIScreen mainScreen] currentMode].size.width,(int)[[UIScreen mainScreen] currentMode].size.height];
    
    srand((int)[[NSDate date] timeIntervalSince1970]);
    int nonce = rand();
    
    NSString *key = @"hunterxhunter";
#if !(TARGET_IPHONE_SIMULATOR)
    NSString *rquestStr = [NSString stringWithFormat:@"a0=%@&a1=%@&a2=%@&a3=1&a5=%@&a6=%lu&a7=%@&a9=%d&a8=", uid, resultAlbums, version, lang, ios, resolution, nonce];
#else
    NSString *rquestStr = [NSString stringWithFormat:@"a0=%@&a1=%@&a2=%@&a3=1&a4=1&a5=%@&a6=%lu&a7=%@&a9=%d&a8=", uid, resultAlbums, version, lang, ios, resolution, nonce];
#endif
    const char *cData = [rquestStr cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    
    NSString *hash = [utility encodeToPercentEscapeString:[HMAC base64Encoding]];
    rquestStr = [rquestStr stringByAppendingString:hash];
    NSString* requestURL = [NSString stringWithFormat:@"http://www.apprate.net/al.php?%@", rquestStr] ;
    
    NSLog(@"%@", requestURL);

    if (insDelegate == nil) {
        insDelegate = [[InstallReportDelegate alloc]init];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestURL]];
    [[NSURLConnection alloc] initWithRequest:request delegate:insDelegate];
    
}

+ (void) reportForeground
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* uid = [userDefaults valueForKey:@"userid"];
    
    if (!uid) {
        CFUUIDRef   uuidObj = CFUUIDCreate(nil);
        uid = (NSString*)CFUUIDCreateString(nil, uuidObj);
        CFRelease(uuidObj);
        [userDefaults setObject:uid forKey:@"userid"];
    }
    srand((int)[[NSDate date] timeIntervalSince1970]);
    int nonce = rand();
    
    NSString *documentsDir = [utility getDocumentSubPath:@"Video"];
	NSMutableArray *fileList = [NSMutableArray arrayWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDir error:nil]];
    unsigned long nVideo = [fileList count];
    documentsDir = [utility getDocumentSubPath:@"Music"];
    fileList = [NSMutableArray arrayWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDir error:nil]];
    unsigned long nMusic = [fileList count];
    documentsDir = [utility getDocumentSubPath:@"Other"];
    fileList = [NSMutableArray arrayWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDir error:nil]];
    unsigned long nOther = [fileList count];
    
    NSUserDefaults *userDefaut = [NSUserDefaults standardUserDefaults];
    unsigned long nCountMusic = [userDefaut integerForKey:@"MUSIC_REPORT"];
    unsigned long nCountVideo = [userDefaut integerForKey:@"VIDEO_REPORT"];
    unsigned long nCountPDFReader = [userDefaut integerForKey:@"PDF_READER_REPORT"];
    
    NSString *key = @"hunterxhunter";
#if !(TARGET_IPHONE_SIMULATOR)
    NSString *rquestStr = [NSString stringWithFormat:@"a0=%@&a9=%d&f1=%lu&f2=%lu&f3=%lu&f4=%lu&f5=%lu&f6=%lu&a8=", uid, nonce, nVideo, nMusic, nOther, nCountVideo, nCountMusic, nCountPDFReader];
#else
    NSString *rquestStr = [NSString stringWithFormat:@"a0=%@&a9=%d&f1=%lu&f2=%lu&f3=%lu&f4=%lu&f5=%lu&f6=%lu&a4=1&a8=", uid, nonce, nVideo, nMusic, nOther, nCountVideo, nCountMusic, nCountPDFReader];
#endif
    
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [rquestStr cStringUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    
    NSString *hash = [utility encodeToPercentEscapeString:[HMAC base64Encoding]];
    rquestStr = [rquestStr stringByAppendingString:hash];
    
    NSString* requestURL = [NSString stringWithFormat:@"http://www.apreview.org/al1.php?%@", rquestStr] ;
    
    NSLog(@"%@", requestURL);
    
    if (foreDelegate == nil) {
        foreDelegate = [[ForegroundReportDelegate alloc]init];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestURL]];
    [[NSURLConnection alloc] initWithRequest:request delegate:foreDelegate];
    
}

@end

@implementation InstallReportDelegate

- (void)connection:(NSURLConnection*) conn didReceiveData:(NSData *)data
{
    NSString* result = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", result);
    NSRange range = [result rangeOfString:@"ok"];
    unsigned long index = range.location + range.length;
    if (index <= [result length]) {
        NSUserDefaults *userDefaut = [NSUserDefaults standardUserDefaults];
        [userDefaut setInteger:1 forKey:@"INSTALL_REPORT"];
        
        [userDefaut synchronize];
    }
}
@end

@implementation ForegroundReportDelegate

- (void)connection:(NSURLConnection*) conn didReceiveData:(NSData *)data
{
    NSString* result = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", result);
    NSRange range = [result rangeOfString:@"ok"];
    unsigned long index = range.location + range.length;
    if (index <= [result length]) {
        NSUserDefaults *userDefaut = [NSUserDefaults standardUserDefaults];
        [userDefaut setInteger:0 forKey:@"PDF_READER_REPORT"];
        [userDefaut setInteger:0 forKey:@"VIDEO_REPORT"];
        [userDefaut setInteger:0 forKey:@"MUSIC_REPORT"];
    }
}

@end
