//
//  NavigationalSubView.h
//  FlyTransfer
//
//  Created by  apple on 13-7-23.
//  Copyright 2013 Calibri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NavigationalDelegate.h"


@interface NavigationalSubTableView : UITableViewController {
	id<NavigationalDelegate> nav_delegate;
}

@property (nonatomic, assign) id <NavigationalDelegate> nav_delegate;

- (IBAction)done:(id)sender;

@end
