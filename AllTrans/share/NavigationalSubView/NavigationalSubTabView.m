//
//  NavigationalSubView.m
//  FlyTransfer
//
//  Created by  apple on 13-7-23.
//  Copyright 2013 Calibri. All rights reserved.
//

#import "NavigationalSubTabView.h"
#import "UICustomization.h"
#import "utility.h"

@implementation NavigationalSubTabView
@synthesize nav_delegate;
@synthesize navigationBar;

- (IBAction)done:(id)sender {
	[self.nav_delegate viewControllerDidFinish:self];	
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [UICustomization setNVBar:navigationBar Item:navigationBar.topItem Target:self action:@selector(done:) Image:@"back.png" IsLeft:YES];
    
    if ([utility DeviceSystemMajorVersion] < 7) {
        CGRect frame = navigationBar.frame;
        frame.origin.y -= 20;
        navigationBar.frame = frame;
    }
}

@end
