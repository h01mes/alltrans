//
//  NavigationalSubView.m
//  FlyTransfer
//
//  Created by  apple on 13-7-23.
//  Copyright 2013 Calibri. All rights reserved.
//

#import "NavigationalSubTableView.h"
#import "UICustomization.h"

@implementation NavigationalSubTableView
@synthesize nav_delegate;

- (IBAction)done:(id)sender {
	[self.nav_delegate viewControllerDidFinish:self];	
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [UICustomization setNVBar:self.navigationController.navigationBar Item:[self navigationItem] Target:self action:@selector(done:) Image:@"back.png" IsLeft:YES];
}

@end
