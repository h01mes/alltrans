//
//  NavigaionalDelegate.h
//  FlyTransfer
//
//  Created by  apple on 13-7-23.
//  Copyright 2013 Calibri. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol NavigationalDelegate
- (void)viewControllerDidFinish:(UIViewController *)controller;

@end

