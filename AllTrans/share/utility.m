//
//  utility.m
//  FlyTransfer
//
//  Created by  apple on 13-8-18.
//  Copyright 2013 Calibri. All rights reserved.
//

#import "utility.h"
#import <ifaddrs.h>
#import <arpa/inet.h>

NSString* documentPath = nil;


@implementation utility

+ (NSString*) getDocumentPath
{
	if (documentPath == nil) {
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		documentPath = [[NSString alloc] initWithFormat:@"%@", [[paths objectAtIndex:0] stringByAppendingPathComponent:@"web"]];
	}
	return documentPath;
}

+ (NSString*) getDocumentSubPath:(NSString*)subDir
{
	if (documentPath == nil) {
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		
		documentPath = [[NSString alloc] initWithFormat:@"%@", [[paths objectAtIndex:0] stringByAppendingPathComponent:@"web"]];
		//		documentPath = [NSString alloc] initWithFormat:@"" [];
	}
	
	NSString* subPath = [documentPath stringByAppendingPathComponent:subDir];
	return subPath;
}

+ (NSString *)encodeToPercentEscapeString: (NSString *) input
{
    // Encode all the reserved characters, per RFC 3986
    // (<http://www.ietf.org/rfc/rfc3986.txt>)
    NSString *outputStr = (NSString *)
    CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                            (CFStringRef)input,
                                            NULL,
                                            (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                            kCFStringEncodingUTF8);
    return [outputStr autorelease];
}  

+ (NSString *)decodeFromPercentEscapeString: (NSString *) input
{
    NSMutableString *outputStr = [NSMutableString stringWithString:input];
    [outputStr replaceOccurrencesOfString:@"+"
                               withString:@" "
                                  options:NSLiteralSearch
                                    range:NSMakeRange(0, [outputStr length])];
    return [outputStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}  

+ (void)copyFromAsset:(ALAsset*)asset toFile:(NSString*) filepath;
{
	NSFileManager *fileManager = [NSFileManager defaultManager];

	NSLog(@"save tmp file:%@",filepath);
	[fileManager createFileAtPath:filepath contents:nil attributes:nil];
	NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:filepath];
	if (!handle)
	{
	}
	
	static const NSUInteger BufferSize = 1024*1024;
	ALAssetRepresentation *rep = [asset defaultRepresentation];
	uint8_t *buffer = calloc(BufferSize, sizeof(*buffer));
	NSUInteger offset = 0, bytesRead = 0;

	// Read the buffer and write the data to your destination path as you go
	do
	{
		@try
		{
			bytesRead = [rep getBytes:buffer fromOffset:offset length:BufferSize error:nil];
			[handle writeData:[NSData dataWithBytesNoCopy:buffer length:bytesRead freeWhenDone:NO]];
			offset += bytesRead;
		}
		@catch (NSException *exception)
		{
			free(buffer);
		
			// Handle the exception here...
		}
	} while (bytesRead > 0);
	free(buffer);
	[handle closeFile];
}

+ (NSString *)getIPAddress
{    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
	
    // retrieve the current interfaces - returns 0 on success
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]; // pdp_ip0     
                NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
				
                if([name isEqualToString:@"en0"]) {
                    // Interface is the wifi connection on the iPhone
                    wifiAddress = addr;    
                } else
					if([name isEqualToString:@"pdp_ip0"]) {
						// Interface is the cell connection on the iPhone
						cellAddress = addr;    
					}
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    NSString *addr = wifiAddress ? wifiAddress : cellAddress;
    return addr ? addr : @"0.0.0.0";
}

+ (void)truncateDir:(NSString*) pathname
{
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:pathname error:nil];
    for (NSObject* elem in fileList)
    {
        [fileManager removeItemAtPath:[pathname stringByAppendingPathComponent:(NSString*)elem] error:nil];
    }
    return;
}

+ (void)truncateResourceAtDir:(NSString*) pathname
{
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:pathname error:nil];
    for (NSObject* elem in fileList)
    {
        NSString* fileName = (NSString*)elem;
        NSString* fullPath = [pathname stringByAppendingPathComponent:(NSString*)elem];
        BOOL* isDirectory = FALSE;
        if ([fileManager fileExistsAtPath:fullPath isDirectory:isDirectory]) {
            if (!isDirectory) {
                [fileManager removeItemAtPath:fullPath error:nil];
            }
            else
            {
                if (![fileName isEqualToString:@"MyUpload"] && ![fileName isEqualToString:NSLocalizedString(@"Video", @"Video")] && ![fileName isEqualToString:NSLocalizedString(@"Music", @"Music")] && ![fileName isEqualToString:NSLocalizedString(@"Other", @"Other")] && ![fileName isEqualToString:@"thumbnails"] && ![fileName isEqualToString:@"tmp"]) {
 //                   [utility truncateDir:fullPath];
                    [fileManager removeItemAtPath:fullPath error:nil];
                }
            }
        }
    }
    return;
}

+ (void)copyResourceAtDir:(NSString*) pathname from:(NSString*) resourceDir
{
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:resourceDir error:nil];
    for (NSObject* elem in fileList)
    {
//        NSString* fileName = (NSString*)elem;
        NSString* fullPath = [pathname stringByAppendingPathComponent:(NSString*)elem];
        NSString* resourcePath = [resourceDir stringByAppendingPathComponent:(NSString*)elem];
        [fileManager copyItemAtPath:resourcePath toPath:fullPath error:nil];

    }
    return;
}

+ (NSUInteger) DeviceSystemMajorVersion
{
    static NSUInteger _deviceSystemMajorVersion = -1;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _deviceSystemMajorVersion = [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue];
    });
    return _deviceSystemMajorVersion;
}

@end
