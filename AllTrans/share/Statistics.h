//
//  Statistics.h
//  flytransfer
//
//  Created by Holmes on 11/7/13.
//
//

#import <Foundation/Foundation.h>


@interface InstallReportDelegate : NSObject
- (void)connection:(NSURLConnection*) conn didReceiveData:(NSData *)data;
@end

@interface ForegroundReportDelegate : NSObject
- (void)connection:(NSURLConnection*) conn didReceiveData:(NSData *)data;
@end

@interface Statistics : NSObject{

}

+ (void) reportForeground;
+ (void) reportInstall;
@end
