//
//  UICustomization.h
//  FlyTransfer
//
//  Created by  apple on 13-7-23.
//  Copyright 2013 Calibri. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UICustomization : NSObject {

}

+ (void) setBackGround:(UIViewController*)selfController Image:(NSString*)imageName;

+ (void) setNVBar:(UINavigationBar*)selfBar Item:(UINavigationItem*)selfItem Target:(id)target action:(SEL)action Image:(NSString*)imageName IsLeft:(BOOL)isleft;

+ (void) setNVBar:(UINavigationBar*)selfBar Item:(UINavigationItem*)selfItem Button:(UIBarButtonItem*)barButtonItem IsLeft:(BOOL)isleft;


+ (void) setTabBar:(UITabBar*)tabBar withColor:(UIColor*)color andWidth:(int)width andHeight:(int)height;

+ (void) setTabBar:(UITabBar*)tabBar withImage:(NSString*)imageName andWidth:(int)width andHeight:(int)height;

+ (void) setNavText:(NSString*) title item:(UINavigationItem*) navigationItem;

@end
