//
//  WTZHTTPConnection.h
//  FileServer
//
//  Created by willonboy on 11-12-7.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "HTTPConnection.h"

extern int uploadCount;

@interface WTZHTTPConnection : HTTPConnection
{
	long             dataStartIndex;
	NSMutableArray  *multipartData;
	BOOL            postHeaderOK;
	int				isFile;
	NSString *      fileName;
}

//- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
- (BOOL) isBeginOfOctetStream;
- (BOOL) shouldDie;

+ (NSString *)getUploading;


@end
