//
//  WTZHTTPConnection.m
//  FileServer
//
//  Created by willonboy on 11-12-7.(qq:962286684)
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <MobileCoreServices/UTType.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import "WTZHTTPConnection.h"
#import "HTTPDynamicFileResponse.h"
#import	"HTTPFileResponse.h"
#import "URLParser.h"
#import "HTTPMessage.h"
#import "UIImage+KTCategory.h"
#import "ZipArchive.h"
#import "utility.h"
#import "DirInfo.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "HTTPDataResponse.h"
#import "DirManager.h"

#define itemsPerPage 20
#define showPages 5

static NSString *      staticLastObj;

NSObject* lockForUploadCount;
int uploadCount = 0;

@implementation WTZHTTPConnection

- (void)dealloc {
    [multipartData release];
    [super dealloc];
}


+ (NSString *)getUploading
{
    return staticLastObj;
}


    //扩展HTTPServer支持的请求类型，默认支持GET，HEAD，不支持POST
- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)relativePath
{
	if ([@"POST" isEqualToString:method])
	{
		return YES;
	}
	return [super supportsMethod:method atPath:relativePath];
}

- (NSData *)preprocessResponse:(HTTPMessage *)response
{
	if (isFile) {
        NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                NULL,
                                                                                (CFStringRef)fileName,
                                                                                NULL,
                                                                                (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                kCFStringEncodingUTF8 );
		[response setHeaderField:@"Content-disposition" value:[@"attachment; filename*=utf8' '" stringByAppendingString:encoded]];
        [encoded autorelease];
		switch (isFile) {
			case 3:
				[response setHeaderField:@"Content-type" value:@"application/zip"];
				break;
			case 1:
				[response setHeaderField:@"Content-type" value:@"image/png"];
				break;
            case 2:
                [response setHeaderField:@"Content-type" value:@"audio/mpeg"];
                break;
            case 4:
//                [response setHeaderField:@"Content-type" value:@"video/mpeg"];
                break;
            case 5:
                [response setHeaderField:@"Content-type" value:@""];
                break;
			default:
				break;
		}
		
	}
	return [super preprocessResponse:response];
}


    //处量返回的response数据
- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path
{
	// Use HTTPConnection's filePathForURI method.
	// This method takes the given path (which comes directly from the HTTP request),
	// and converts it to a full path by combining it with the configured document root.
	// 
	// It also does cool things for us like support for converting "/" to "/index.html",
	// and security restrictions (ensuring we don't serve documents outside configured document root folder).

	
	NSString *filePath = [self filePathForURI:path];
	
	// Convert to relative path
	
	NSString *documentRoot = [config documentRoot];
	
	if (![filePath hasPrefix:documentRoot])
	{
		// Uh oh.
		// HTTPConnection's filePathForURI was supposed to take care of this for us.
		return nil;
	}
	
	NSString *relativePath = [filePath substringFromIndex:[documentRoot length]];
	
	isFile = 0;
	URLParser *parser = [[[URLParser alloc] initWithURLString:path] autorelease];
	NSString *isDownload = [parser valueForVariable:@"download"];
	if (isDownload != nil)
	{
		isFile = 1;
	}
	
	NSRange range = [relativePath rangeOfString:@"Picture/"];
//    NSString* token = [@"Picture" stringByAppendingString:@"/"];
//	NSRange range = [relativePath rangeOfString:token];
	long index = range.location + range.length;
	if (index <= [relativePath length]) {
		NSString* libraryPath = [relativePath substringFromIndex:index];
		
		range = [libraryPath rangeOfString:@"/"];
		if (range.location > [libraryPath length]) {
			goto outif;
		}
		NSString* albumName = [libraryPath substringToIndex:range.location];
		index = range.location + range.length;
		
		if (index > [libraryPath length]) {
			goto outif;
		}
		NSString* picName = [libraryPath substringFromIndex:index];
		NSLog(@"photo libraryPath:%@", libraryPath);
		DirInfo* thisDir = [[DirManager getDirMap] objectForKey:albumName];
		if (thisDir != nil) {
			int assetIndex = [picName intValue];
			if (assetIndex >= thisDir.fileCount)
            {
				//goto outif;
                UIImage *i = [UIImage imageNamed:@"photo-icon.png"];
                return [[HTTPDataResponse alloc] initWithData:UIImagePNGRepresentation(i)];
            }
			
			ALAsset* thisasset = [thisDir.assets objectAtIndex:assetIndex];
            
			if (isFile) {
                if (thisasset != nil && [ALAssetTypeVideo isEqualToString:[thisasset valueForProperty:ALAssetPropertyType]])
                    isFile = 4;
                else
                    isFile = 1;
                fileName = [relativePath substringFromIndex:index];
				ALAssetRepresentation *rep = [thisasset defaultRepresentation];
				Byte *buffer = (Byte*)malloc((unsigned long)rep.size);
				NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:(unsigned int)rep.size error:nil];
				NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];//leak
				return [[HTTPDataResponse alloc] initWithData:data];
			}
			else {
				UIImage* thisImage = [UIImage imageWithCGImage:[thisasset thumbnail]];
				return [[HTTPDataResponse alloc] initWithData:UIImagePNGRepresentation(thisImage)];
			}
		}
	}
    range = [relativePath rangeOfString:@"Music/"];
//    token = [@"Music" stringByAppendingString:@"/"];
//    range = [relativePath rangeOfString:token];
    index = range.location + range.length;
    if (index <= [relativePath length])
    {
        if (isFile) {
            fileName = [relativePath substringFromIndex:index];
            isFile = 2;
        }
    }
    range = [relativePath rangeOfString:@"Video/"];
//    token = [@"Video" stringByAppendingString:@"/"];
//    range = [relativePath rangeOfString:token];
    index = range.location + range.length;
    if (index <= [relativePath length])
    {
        if (isFile) {
            fileName = [relativePath substringFromIndex:index];
            isFile = 4;
        }
    }
    range = [relativePath rangeOfString:@"Other/"];
//    token = [@"Other" stringByAppendingString:@"/"];
//    range = [relativePath rangeOfString:token];
    index = range.location + range.length;
    if (index <= [relativePath length])
    {
        if (isFile) {
            fileName = [relativePath substringFromIndex:index];
            isFile = 5;
        }
    }    
	
outif:
	
	if ([relativePath isEqualToString:@"/index.html"])
	{
		NSFileManager *fileManager = [NSFileManager defaultManager];
		BOOL isDir;
		
		NSString *documentsDir = [utility getDocumentSubPath:@"Picture"];
		NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:documentsDir];
		NSString *file;
		NSString *content = @"";
		
		while ((file = [dirEnum nextObject])) {
			
			BOOL isLibrary = FALSE;
			
			DirInfo* thisDir = [[DirManager getDirMap] objectForKey:file];
			if (thisDir != nil) {
				isLibrary = TRUE;
			}
			
			NSString *thumbDir = @"Picture";

			NSString *fulPath = [documentsDir stringByAppendingPathComponent:file];
			if ([fileManager fileExistsAtPath:fulPath isDirectory:&isDir] && isDir) {
				// process the document
				
				NSString *queryString= [@"photo.html?dir=" stringByAppendingString:[utility encodeToPercentEscapeString:file]];
				
				if (!isLibrary) {
					NSDirectoryEnumerator *dirEnumInner = [fileManager enumeratorAtPath:fulPath];
					NSString *innerFile = [dirEnumInner nextObject];
					thumbDir = [thumbDir stringByAppendingPathComponent:file];
					thumbDir = innerFile != nil ? [thumbDir stringByAppendingPathComponent:innerFile] : [thumbDir stringByAppendingPathComponent:@"nil"];
				}
				else {
					thumbDir = [fulPath stringByAppendingPathComponent:@"0.png"];
				}

				content = [content stringByAppendingString: @"<div class='photo'>"\
				"<div class='photo-cell'>" \
				"<ul>" \
				"<li>" \
					"<div class='photo-cell-img'>" \
					"<a href='"];
				
				content = [content stringByAppendingString:queryString];
				content = [content stringByAppendingString:@"'><img src='"];
				content = [content stringByAppendingString:thumbDir];
				content = [content stringByAppendingString:@"' width='126px' height='126px'/></a>"\
					"</div>"\
				"</li>"\
				"<li class='photo-cell-til'><a href='"];
				content = [content stringByAppendingString:queryString];
				content = [content stringByAppendingString:@"'>"];
				content = [content stringByAppendingString:file];
				content = [content stringByAppendingString:@"</a>"\
				"</li>"\
				"</ul>"\
				"</div>"];
			}
		}
		
		NSMutableDictionary *replacementDict = [NSMutableDictionary dictionaryWithCapacity:5];
		
		[replacementDict setObject:content forKey:@"DynamicContent"];
        
        [replacementDict setObject:NSLocalizedString(@"Picture", @"Picture") forKey:@"Photo"];
        [replacementDict setObject:NSLocalizedString(@"Music", @"Music") forKey:@"Music"];
        [replacementDict setObject:NSLocalizedString(@"Video", @"Video") forKey:@"Video"];
        [replacementDict setObject:NSLocalizedString(@"Other", @"Other") forKey:@"File"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString1", @"Select your files and transfer to iOS device") forKey:@"LocalizedString1"];
				
		return [[[HTTPDynamicFileResponse alloc] initWithFilePath:[self filePathForURI:path]
		                                            forConnection:self
		                                                separator:@"%%"
		                                    replacementDictionary:replacementDict] autorelease];
	}
	else if ([relativePath isEqualToString:@"/photo.html"])
	{
		NSString *dir = [utility decodeFromPercentEscapeString:[parser valueForVariable:@"dir"]];
		if (dir == nil) {
			return [super httpResponseForMethod:method URI:path]; 
		}
		NSString *page = [parser valueForVariable:@"page"];
		NSLog(@"%@:%@", dir, page);
		
		BOOL isLibrary = FALSE;
		
		DirInfo* thisDir = [[DirManager getDirMap] objectForKey:dir];
		if (thisDir != nil) {
			isLibrary = TRUE;
		}
		
//		NSArray *dirEnum = nil;
		int totalPage = 0;
		int totalNum = 0;
		if (isLibrary) {
			totalNum = thisDir.fileCount;
			totalPage = thisDir.fileCount/itemsPerPage + 1;
		}
		else {
            
            //go error
		}
		
		int curPage;
		if (page == nil) {
			curPage = 1;
			page = @"1";
		}
		else {
			curPage = [page intValue];
		}
		
		NSString *file;
		NSString *content = @"";
		int iCount = itemsPerPage*(curPage-1);
		int iTh = 0;
		while (iTh < itemsPerPage && iCount < totalNum) {
            bool isVideo = false;
			if (!isLibrary) {
				//file = [dirEnum objectAtIndex:iCount];
                //go error
			}
			else {
                ALAsset* thisAsset = [thisDir.assets objectAtIndex:iCount];
                if (thisAsset != nil && [ALAssetTypeVideo isEqualToString:[thisAsset valueForProperty:ALAssetPropertyType]])
                    isVideo = true;
                
                if (isVideo) {
                    file = [NSString stringWithFormat:@"%d.mov", iCount];
                }
                else
                {
                    file = [NSString stringWithFormat:@"%d.png", iCount];
                }
			}

			
			NSString *thumbDir = [[@"Picture" stringByAppendingPathComponent:dir] stringByAppendingPathComponent: file];
			NSString *inputid = [[NSString alloc] initWithFormat:@"input%d",iTh]; 
			
            if (isVideo)
            {
                content = [content stringByAppendingString:@"<div class='video-cell pic-content-cell'><span></span>"\
                           "<img src='"];
            }
            else
            {
                content = [content stringByAppendingString:@"<div class='video-cell pic-content-cell'>"\
                           "<img src='"];
            }

			content = [content stringByAppendingString:thumbDir];
			content = [content stringByAppendingString:@"' width='102px' height='102px' />"];
			content = [content stringByAppendingString:@"<p><input id='"];
			content = [content stringByAppendingString:inputid];
			content = [content stringByAppendingString:@"' type='checkbox' value='' name='' class='chk' onclick='checkTure()'/>&nbsp;"\
					   "<a href='"];
			content = [content stringByAppendingString:thumbDir];
			content = [content stringByAppendingString:@"?download=1'>Download</a></p>"\
					   "</div>"];
			
			iCount++;
			iTh++;
		}
		
		NSString *pageInfo = @"<div class='page'>";
		NSString *queryString= [@"photo.html?dir=" stringByAppendingString:dir];
		queryString = [queryString stringByAppendingString:@"&page="];
		
		if (curPage == 1)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>&lt; pre</a>"];
		}
		else
		{
			NSString * queryStringPre = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage-1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringPre];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>&lt; pre</a>"];
		}

		BOOL isGapFilled = NO;
		for (int i = curPage; i <= totalPage; i++) {
			NSString * currIter = [[NSString alloc] initWithFormat:@"%d",i];
			if (i == curPage) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</span>"];
			}
			else if ((i-curPage) < showPages || i == totalPage) {
				NSString * queryStringCurr = [queryString stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
				pageInfo = [pageInfo stringByAppendingString:queryStringCurr];
				pageInfo = [pageInfo stringByAppendingString:@"' class='number'>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</a>"];
			}
			else if (isGapFilled == NO) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>&middot;&middot;&middot;</span>"];
				isGapFilled = YES;
			}
		}

		if (curPage == totalPage)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>next &gt;</a>"];
		}
		else
		{
			NSString * queryStringNext = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage+1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringNext];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>next &gt;</a>"];
		}
		pageInfo = [pageInfo stringByAppendingString:@"</div>"];
		
		NSMutableDictionary *replacementDict = [NSMutableDictionary dictionaryWithCapacity:5];
		
		[replacementDict setObject:content forKey:@"DynamicContent"];
		[replacementDict setObject:pageInfo forKey:@"DynamicPageInfo"];
		[replacementDict setObject:dir forKey:@"SubDir"];
		[replacementDict setObject:page forKey:@"GetPage"];

        [replacementDict setObject:NSLocalizedString(@"Picture", @"Picture") forKey:@"Photo"];
        [replacementDict setObject:NSLocalizedString(@"Music", @"Music") forKey:@"Music"];
        [replacementDict setObject:NSLocalizedString(@"Video", @"Video") forKey:@"Video"];
        [replacementDict setObject:NSLocalizedString(@"Other", @"Other") forKey:@"File"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString1", @"Select your files and transfer to iOS device") forKey:@"LocalizedString1"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString2", @"select all") forKey:@"LocalizedString2"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString3", @"download") forKey:@"LocalizedString3"];
		
		return [[[HTTPDynamicFileResponse alloc] initWithFilePath:[self filePathForURI:path]
		                                            forConnection:self
		                                                separator:@"%%"
		                                    replacementDictionary:replacementDict] autorelease];
	}
	else if ([relativePath isEqualToString:@"/music.html"]) {
		NSString *page = [parser valueForVariable:@"page"];
		NSLog(@"%@", page);
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		NSString *documentsDir = [utility getDocumentSubPath:@"Music"];
        
		NSArray *dirEnum = [fileManager contentsOfDirectoryAtPath:documentsDir error:nil];
		
		long totalPage = [dirEnum count]/itemsPerPage + 1;

		int curPage;
		if (page == nil) {
			curPage = 1;
			page = @"1";
		}
		else {
			curPage = [page intValue];
		}
		
		NSString *file;
		NSString *content = @"";
		int iCount = itemsPerPage*(curPage-1);
		int iTh = 0;
		while (iTh < itemsPerPage && iCount < [dirEnum count]) {
			file = [dirEnum objectAtIndex:iCount];
			NSString *downloadDir = [@"Music" stringByAppendingPathComponent: file];
			
			NSString *inputid = [[NSString alloc] initWithFormat:@"input%d",iTh]; 
			
        	content = [content stringByAppendingString:@"<div>"\
					   "<span class='list-sel'><input id='"];
			content = [content stringByAppendingString:inputid];
			content = [content stringByAppendingString:@"' type='checkbox' value='' name='' class='chk' onclick='checkTure()'/>"\
					   "</span>"\
					   "<span class='list-content'><img src='images/icon-music.png' />"];
			content = [content stringByAppendingString:file];
			content = [content stringByAppendingString:@"</span>"\
						"<a href='"];
			content = [content stringByAppendingString:downloadDir];
			content = [content stringByAppendingString:@"?download=1' class='list-dow'></a>"\
					   "</div>"];
			
			
			iCount++;
			iTh++;
		}
		
		NSString *pageInfo = @"<div class='page'>";
		NSString *queryString= @"music.html?";
		queryString = [queryString stringByAppendingString:@"&page="];
		
		if (curPage == 1)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>&lt; pre</a>"];
		}
		else
		{
			NSString * queryStringPre = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage-1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringPre];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>&lt; pre</a>"];
		}
		
		BOOL isGapFilled = NO;
		for (int i = curPage; i <= totalPage; i++) {
			NSString * currIter = [[NSString alloc] initWithFormat:@"%d",i];
			if (i == curPage) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</span>"];
			}
			else if ((i-curPage) < showPages || i == totalPage) {
				NSString * queryStringCurr = [queryString stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
				pageInfo = [pageInfo stringByAppendingString:queryStringCurr];
				pageInfo = [pageInfo stringByAppendingString:@"' class='number'>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</a>"];
			}
			else if (isGapFilled == NO) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>&middot;&middot;&middot;</span>"];
				isGapFilled = YES;
			}
		}
		
		if (curPage == totalPage)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>next &gt;</a>"];
		}
		else
		{
			NSString * queryStringNext = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage+1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringNext];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>next &gt;</a>"];
		}
		pageInfo = [pageInfo stringByAppendingString:@"</div>"];
		
		NSMutableDictionary *replacementDict = [NSMutableDictionary dictionaryWithCapacity:5];
		
		[replacementDict setObject:page forKey:@"GetPage"];
		[replacementDict setObject:content forKey:@"DynamicContent"];
		[replacementDict setObject:pageInfo forKey:@"DynamicPageInfo"];
		
        [replacementDict setObject:NSLocalizedString(@"Picture", @"Picture") forKey:@"Photo"];
        [replacementDict setObject:NSLocalizedString(@"Music", @"Music") forKey:@"Music"];
        [replacementDict setObject:NSLocalizedString(@"Video", @"Video") forKey:@"Video"];
        [replacementDict setObject:NSLocalizedString(@"Other", @"Other") forKey:@"File"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString1", @"Select your files and transfer to iOS device") forKey:@"LocalizedString1"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString2", @"select all") forKey:@"LocalizedString2"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString3", @"download") forKey:@"LocalizedString3"];
		
		return [[[HTTPDynamicFileResponse alloc] initWithFilePath:[self filePathForURI:path]
		                                            forConnection:self
		                                                separator:@"%%"
		                                    replacementDictionary:replacementDict] autorelease];
	}
	else if ([relativePath isEqualToString:@"/file.html"]){		
		NSString *page = [parser valueForVariable:@"page"];
		NSLog(@"%@", page);
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		NSString *documentsDir = [utility getDocumentSubPath:@"Other"];
		NSArray *dirEnum = [fileManager contentsOfDirectoryAtPath:documentsDir error:nil];
		
		long totalPage = [dirEnum count]/itemsPerPage + 1;

		int curPage;
		if (page == nil) {
			curPage = 1;
			page = @"1";
		}
		else {
			curPage = [page intValue];
		}
		
		NSString *file;
		NSString *content = @"";
		int iCount = itemsPerPage*(curPage-1);
		int iTh = 0;
		while (iTh < itemsPerPage && iCount < [dirEnum count]) {
			file = [dirEnum objectAtIndex:iCount];
			NSString *downloadDir = [@"Other" stringByAppendingPathComponent: file];
			
			NSString *inputid = [[NSString alloc] initWithFormat:@"input%d",iTh]; 
			
        	content = [content stringByAppendingString:@"<div>"\
					   "<span class='list-sel'><input id='"];
			content = [content stringByAppendingString:inputid];
			content = [content stringByAppendingString:@"' type='checkbox' value='' name='' class='chk' onclick='checkTure()'/>"\
					   "</span>"\
					   "<span class='list-content'><img src='images/icon-file.png' />"];
			content = [content stringByAppendingString:file];
			content = [content stringByAppendingString:@"</span>"\
					   "<a href='"];
			content = [content stringByAppendingString:downloadDir];
			content = [content stringByAppendingString:@"?download=1' class='list-dow'></a>"\
					   "</div>"];
			
			iCount++;
			iTh++;
		}
		
		NSString *pageInfo = @"<div class='page'>";
		NSString *queryString= @"file.html?";
		queryString = [queryString stringByAppendingString:@"&page="];
		
		if (curPage == 1)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>&lt; pre</a>"];
		}
		else
		{
			NSString * queryStringPre = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage-1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringPre];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>&lt; pre</a>"];
		}
		
		BOOL isGapFilled = NO;
		for (int i = curPage; i <= totalPage; i++) {
			NSString * currIter = [[NSString alloc] initWithFormat:@"%d",i];
			if (i == curPage) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</span>"];
			}
			else if ((i-curPage) < showPages || i == totalPage) {
				NSString * queryStringCurr = [queryString stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
				pageInfo = [pageInfo stringByAppendingString:queryStringCurr];
				pageInfo = [pageInfo stringByAppendingString:@"' class='number'>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</a>"];
			}
			else if (isGapFilled == NO) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>&middot;&middot;&middot;</span>"];
				isGapFilled = YES;
			}
		}
		
		if (curPage == totalPage)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>next &gt;</a>"];
		}
		else
		{
			NSString * queryStringNext = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage+1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringNext];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>next &gt;</a>"];
		}
		pageInfo = [pageInfo stringByAppendingString:@"</div>"];
		
		NSMutableDictionary *replacementDict = [NSMutableDictionary dictionaryWithCapacity:5];
		
		[replacementDict setObject:page forKey:@"GetPage"];
		[replacementDict setObject:content forKey:@"DynamicContent"];
		[replacementDict setObject:pageInfo forKey:@"DynamicPageInfo"];
		
        [replacementDict setObject:NSLocalizedString(@"Picture", @"Picture") forKey:@"Photo"];
        [replacementDict setObject:NSLocalizedString(@"Music", @"Music") forKey:@"Music"];
        [replacementDict setObject:NSLocalizedString(@"Video", @"Video") forKey:@"Video"];
        [replacementDict setObject:NSLocalizedString(@"Other", @"Other") forKey:@"File"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString1", @"Select your files and transfer to iOS device") forKey:@"LocalizedString1"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString2", @"select all") forKey:@"LocalizedString2"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString3", @"download") forKey:@"LocalizedString3"];
		
		return [[[HTTPDynamicFileResponse alloc] initWithFilePath:[self filePathForURI:path]
		                                            forConnection:self
		                                                separator:@"%%"
		                                    replacementDictionary:replacementDict] autorelease];
	}
	else if ([relativePath isEqualToString:@"/video.html"]){		
		NSString *page = [parser valueForVariable:@"page"];
		NSLog(@"%@", page);
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		NSString *documentsDir = [utility getDocumentSubPath:@"Video"];
        NSString *thumbnailDir = [utility getDocumentSubPath:@"thumbnails"];

		NSArray *dirEnum = [fileManager contentsOfDirectoryAtPath:documentsDir error:nil];
		
		long totalPage = [dirEnum count]/itemsPerPage + 1;

		int curPage;
		if (page == nil) {
			curPage = 1;
			page = @"1";
		}
		else {
			curPage = [page intValue];
		}
		
		NSString *file;
		NSString *content = @"";
		int iCount = itemsPerPage*(curPage-1);
		int iTh = 0;
		while (iTh < itemsPerPage && iCount < [dirEnum count]) {
			file = [dirEnum objectAtIndex:iCount];
			NSString *downloadDir = [@"Video" stringByAppendingPathComponent: file];
			NSString *downloadDir4Thumbnail = [@"thumbnails" stringByAppendingPathComponent: file];
			NSString *realDir = [documentsDir stringByAppendingPathComponent:file];
			NSString *thumbnailFile = [thumbnailDir stringByAppendingPathComponent:file];
			
			NSURL *url =[NSURL fileURLWithPath:realDir];
			AVURLAsset *asset = [[[AVURLAsset alloc] initWithURL:url 
														 options:[NSDictionary dictionaryWithObjectsAndKeys:
																  [NSNumber numberWithBool:YES], 
																  AVURLAssetPreferPreciseDurationAndTimingKey,
																  nil]] autorelease];
			
			NSTimeInterval durationInSeconds = 0.0;
			if (asset) 
				durationInSeconds = CMTimeGetSeconds(asset.duration) ;
			
			NSDate *date = [NSDate dateWithTimeIntervalSince1970:durationInSeconds];    
			NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
			[dateFormatter setDateFormat:@"HH:mm:ss"];
			[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
			NSString *formattedDate = [dateFormatter stringFromDate:date];
			
			AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
			NSError *err = NULL;
			generate.appliesPreferredTrackTransform = YES;
			CMTime time = CMTimeMakeWithSeconds(3.0, 1);
			CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];

			[generate release];
			NSLog(@"err==%@, imageRef==%@", err, imgRef);
			if (err == nil)
			{
				UIImage *currentImg = [[[UIImage alloc] initWithCGImage:imgRef] autorelease];
				NSData *tmpData =   UIImageJPEGRepresentation(currentImg, 0.8);
				NSString *path = [NSString stringWithFormat:@"%@~pic", thumbnailFile];
				BOOL ret = [tmpData writeToFile:path atomically:YES]; 
				NSLog(@"write to path=%@, flag=%d", path, ret);
			}

			NSString *inputid = [[NSString alloc] initWithFormat:@"input%d",iTh]; 
			
        	content = [content stringByAppendingString:@"<div class='video-cell pic-content-cell'>"\
					   "<span>"];
			content = [content stringByAppendingString:formattedDate];
			content = [content stringByAppendingString:@"</span>"];
			content = [content stringByAppendingString:@"<img src='"];
			
			content = [content stringByAppendingString:[downloadDir4Thumbnail stringByAppendingString:@"~pic"]];
			
			content = [content stringByAppendingString:@"' width='102px' height='102px' />"\
					   "<p><input id='"];
			content = [content stringByAppendingString:inputid];
			content = [content stringByAppendingString:@"' type='checkbox' value='' name='' class='chk'/>&nbsp;&nbsp;"\
					   "<a href='"];
			content = [content stringByAppendingString:downloadDir];
			content = [content stringByAppendingString:@"?download=1'>Download</a></p>"\
					   "</div>"];
            
			iCount++;
			iTh++;
		}
		
		NSString *pageInfo = @"<div class='page'>";
		NSString *queryString= @"video.html?";
		queryString = [queryString stringByAppendingString:@"&page="];
		
		if (curPage == 1)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>&lt; pre</a>"];
		}
		else
		{
			NSString * queryStringPre = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage-1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringPre];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>&lt; pre</a>"];
		}
		
		BOOL isGapFilled = NO;
		for (int i = curPage; i <= totalPage; i++) {
			NSString * currIter = [[NSString alloc] initWithFormat:@"%d",i];
			if (i == curPage) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</span>"];
			}
			else if ((i-curPage) < showPages || i == totalPage) {
				NSString * queryStringCurr = [queryString stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
				pageInfo = [pageInfo stringByAppendingString:queryStringCurr];
				pageInfo = [pageInfo stringByAppendingString:@"' class='number'>"];
				pageInfo = [pageInfo stringByAppendingString:currIter];
				pageInfo = [pageInfo stringByAppendingString:@"</a>"];
			}
			else if (isGapFilled == NO) {
				pageInfo = [pageInfo stringByAppendingString:@"<span>&middot;&middot;&middot;</span>"];
				isGapFilled = YES;
			}
		}
		
		if (curPage == totalPage)
		{
			pageInfo = [pageInfo stringByAppendingString:@"<a href='#' class='pre-next'>next &gt;</a>"];
		}
		else
		{
			NSString * queryStringNext = [queryString stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",curPage+1]];
			pageInfo = [pageInfo stringByAppendingString:@"<a href='"];
			pageInfo = [pageInfo stringByAppendingString:queryStringNext];
			pageInfo = [pageInfo stringByAppendingString:@"' class='pre-next'>next &gt;</a>"];
		}
		pageInfo = [pageInfo stringByAppendingString:@"</div>"];
		
		NSMutableDictionary *replacementDict = [NSMutableDictionary dictionaryWithCapacity:5];
		
		[replacementDict setObject:page forKey:@"GetPage"];
		[replacementDict setObject:content forKey:@"DynamicContent"];
		[replacementDict setObject:pageInfo forKey:@"DynamicPageInfo"];
        
        [replacementDict setObject:NSLocalizedString(@"Picture", @"Picture") forKey:@"Photo"];
        [replacementDict setObject:NSLocalizedString(@"Music", @"Music") forKey:@"Music"];
        [replacementDict setObject:NSLocalizedString(@"Video", @"Video") forKey:@"Video"];
        [replacementDict setObject:NSLocalizedString(@"Other", @"Other") forKey:@"File"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString1", @"Select your files and transfer to iOS device") forKey:@"LocalizedString1"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString2", @"select all") forKey:@"LocalizedString2"];
        [replacementDict setObject:NSLocalizedString(@"LocalizedString3", @"download") forKey:@"LocalizedString3"];
		
		
		return [[[HTTPDynamicFileResponse alloc] initWithFilePath:[self filePathForURI:path]
		                                            forConnection:self
		                                                separator:@"%%"
		                                    replacementDictionary:replacementDict] autorelease];
	}
	else if ([relativePath isEqualToString:@"/download.zip"]){
		isFile = 3;
		fileName = relativePath;
		NSString *zipFileName = [[utility getDocumentPath] stringByAppendingPathComponent:@"download.zip"];

		NSString *downloadType = [parser valueForVariable:@"downloadtype"];
//		NSString *isDownloadAll = [parser valueForVariable:@"downloadall"];
		ZipArchive *czip = [[ZipArchive alloc] init];
		[czip CreateZipFile2:zipFileName];
		
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		BOOL isLibrary = FALSE;
		NSString *documentDir = nil;
		DirInfo* thisDir = nil;
		
		
		if ([downloadType isEqualToString:@"photo"])
		{
			NSString *subDir = [utility decodeFromPercentEscapeString:[parser valueForVariable:@"subdir"]];
			if (subDir == nil) {
				return [super httpResponseForMethod:method URI:path]; 
			}
			
			thisDir = [[DirManager getDirMap] objectForKey:subDir];
			if (thisDir != nil) {
				isLibrary = TRUE;
			}
			else {
                //go error
			}
		}
		else if ([downloadType isEqualToString:@"video"])
		{
			documentDir = [utility getDocumentSubPath:@"Video"];
		}
		else if ([downloadType isEqualToString:@"music"])
		{
			documentDir = [utility getDocumentSubPath:@"Music"];
		}
		else
		{
			documentDir = [utility getDocumentSubPath:@"Other"];
		}
		
        NSLog(@"%@", documentDir);
		
		if (isLibrary) {
			NSString * tmpDir = [utility getDocumentSubPath:@"tmp"];

			if(![fileManager fileExistsAtPath:tmpDir])
				[fileManager createDirectoryAtPath:tmpDir withIntermediateDirectories:YES attributes:nil error:nil];
			 
/*			if (isDownloadAll != nil) {
				int iCount = 0;
				while (iCount < thisDir.fileCount) {
					NSString * tmpfileName = [NSString stringWithFormat:@"%d.jpg", iCount];
					NSString * tmpfilePath = [tmpDir stringByAppendingPathComponent:tmpfileName];

					[utility copyFromAsset:[thisDir.assets objectAtIndex:iCount] toFile:tmpfilePath];

					[czip addFileToZip:tmpfilePath newname:tmpfileName];
					iCount++;
				}
			}
			else {*/  //only permit downloading current page
				NSString *page = [parser valueForVariable:@"getpage"];
				int curPage;
				if (page == nil) {
					curPage = 1;
				}
				else {
					curPage = [page intValue];
				}
				int iCount = itemsPerPage*(curPage-1);
				
				int iTh = 0;
				while (iTh < itemsPerPage && iCount < [thisDir.assets count])
				{
                    bool isVideo = false;
                    ALAsset* thisAsset = [thisDir.assets objectAtIndex:iCount];
                    if (thisAsset != nil && [ALAssetTypeVideo isEqualToString:[thisAsset valueForProperty:ALAssetPropertyType]])
                        isVideo = true;
                    
                    NSString* extention = @"";
                    if (isVideo) {
                        extention = @"mov";
                    }
                    else
                    {
                        extention = @"png";
                    }
                    
					NSString *checkParam = [NSString stringWithFormat:@"download%d", iTh+1];
					NSString *isChecked = [parser valueForVariable:checkParam];
					if (isChecked != nil) {
						NSString * tmpfileName = [NSString stringWithFormat:@"%d.%@", iCount, extention];
						NSString * tmpfilePath = [tmpDir stringByAppendingPathComponent:tmpfileName];
						
						[utility copyFromAsset:thisAsset toFile:tmpfilePath];
						
						[czip addFileToZip:tmpfilePath newname:tmpfileName];
					}
					iTh++;
					iCount++;
				}
            
            
            [czip CloseZipFile2];
            [czip dealloc];
            
            return [super httpResponseForMethod:method URI:path];
//			}

		}
		else {
			NSString* file;
			NSArray *dirEnum = [fileManager contentsOfDirectoryAtPath:documentDir error:nil];
			
/*			if (isDownloadAll != nil) {
				int iCount = 0;
				while (iCount < [dirEnum count]) {
					file = [dirEnum objectAtIndex:iCount];
					NSString *filePath = [documentDir stringByAppendingPathComponent:file];
					[czip addFileToZip:filePath newname:file];
					iCount++;
				}
			}
			else {*/    //only permit downloading current page
				NSString *page = [parser valueForVariable:@"getpage"];
				int curPage;
				if (page == nil) {
					curPage = 1;
				}
				else {
					curPage = [page intValue];
				}
				int iCount = itemsPerPage*(curPage-1);
				
				int iTh = 0;			
				while (iTh < itemsPerPage && iCount < [dirEnum count])
				{
					NSString *checkParam = [NSString stringWithFormat:@"download%d", iTh+1];
					NSString *isChecked = [parser valueForVariable:checkParam];
					if (isChecked != nil) {
						file = [dirEnum objectAtIndex:iCount];
                        NSString * tmpfileName = [NSString stringWithFormat:@"%d.%@", iCount, [file pathExtension]];
						NSString *filePath = [documentDir stringByAppendingPathComponent:file];//use number temporarily
						[czip addFileToZip:filePath newname:tmpfileName];
					}
					iTh++;
					iCount++;
				}
//			}
			
			
			[czip CloseZipFile2];
			[czip dealloc];
			
			return [super httpResponseForMethod:method URI:path];
		}
	}//batch download

	return [super httpResponseForMethod:method URI:path];
}


static NSString *notificationPostName;

//处理POST请求提交的数据流(下面方法是改自 Andrew Davidson的类)
- (void)processBodyData:(NSData *)postDataChunk
{
 //   NSLog(@"processDataChunk function called");
        //multipartData初始化不放在init函数中, 当前类似乎不经init函数初始化
    if (multipartData == nil) {
        multipartData = [[NSMutableArray alloc] init];
    }
    
	static CFStringRef fileUTI;
    static bool isImage;
	static NSString* filePath = nil;

        //处理multipart/form-data的POST请求中Body数据集中的表单值域并创建文件
	if (!postHeaderOK)
	{
            //0x0A0D: 换行符
		UInt16 separatorBytes = 0x0A0D;
		NSData* separatorData = [NSData dataWithBytes:&separatorBytes length:2];
		
		long l = [separatorData length];

		
		for (int i = 0; i < [postDataChunk length] - l; i++)
		{
                //每次取两个字节 比对下看看是否是换行
			NSRange searchRange = {i, l};
                //如果是换行符则进行如下处理
			if ([[postDataChunk subdataWithRange:searchRange] isEqualToData:separatorData])
			{
                    //获取dataStartIndex标识的上一个换行位置到当前换行符之间的数据的Range
				NSRange newDataRange = {dataStartIndex, i - dataStartIndex};                
                    //dataStartIndex标识的上一个换行位置到移到当前换行符位置 
				dataStartIndex = i + l;
				i += l - 1;
                    //获取dataStartIndex标识的上一个换行位置到当前换行符之间的数据
				NSData *newData = [postDataChunk subdataWithRange:newDataRange];
                    //如果newData不为空或还没有处理完multipart/form-data中表单变量值域则继续处理剩下的表单值域数据
				if ([newData length] || ![self isBeginOfOctetStream])
				{
                    if ([newData length]) {
                        [multipartData addObject:newData];
                    }
				}
				else
				{
                        //将标识处理完multipart/form-data中表单变量值域的postHeaderOK变量设置为TRUE;
					postHeaderOK = TRUE;
                        //这里暂时写成硬编码 弊端:每次增加表单变量都要改这里的数值
                        //获取Content-Disposition: form-data; name="xxx"; filename="xxx"
					NSString* postInfo = [[NSString alloc] initWithBytes:[[multipartData objectAtIndex:4] bytes] 
                                                                  length:[[multipartData objectAtIndex:4] length] 
                                                                encoding:NSUTF8StringEncoding];
                    NSLog(@"postInfo is:%@", postInfo);
					NSArray* postInfoComponents = [postInfo componentsSeparatedByString:@"; filename="];
					postInfoComponents = [[postInfoComponents lastObject] componentsSeparatedByString:@"\""];
                    NSLog(@"postInfoComponents0 :%@",postInfoComponents);
                    if ([postInfoComponents count]<2)
                    {
                        return;
                    }
                    
					postInfoComponents = [[postInfoComponents objectAtIndex:1] componentsSeparatedByString:@"\\"];
					
					CFStringRef fileExtension = (CFStringRef) [[postInfoComponents lastObject] pathExtension];
                    NSString * strFileExtension = (NSString*)fileExtension;
					fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
					
                    NSMutableDictionary* uploadedMap = [DirManager getUploadedMap];

					if (UTTypeConformsTo(fileUTI, kUTTypeImage) && NSOrderedSame != [strFileExtension caseInsensitiveCompare:@"PSD"] && NSOrderedSame != [strFileExtension caseInsensitiveCompare:@"AI"]){
                        isImage = true;
						filePath = [[utility getDocumentSubPath:@"MyUpload"] stringByAppendingPathComponent:[postInfoComponents lastObject]];
						//notificationPostName = @"Saved Photos";
                        NSNumber* num = (NSNumber*)[uploadedMap objectForKey:[NSNumber numberWithInt:0]];
                        if (num == nil) {
                            num = [NSNumber numberWithInt:1];
                        }
                        else
                        {
                            num = [NSNumber numberWithInt:[num intValue]+1];
                        }
                        [uploadedMap setObject:num forKey:[NSNumber numberWithInt:0]];
                        staticLastObj = [[postInfoComponents lastObject] copy];
                        NSLog(@"retain count 1: %lu", (unsigned long)[staticLastObj retainCount]);
                        
					}
					else if (UTTypeConformsTo(fileUTI, kUTTypeAudio))
					{
                        isImage = false;
						filePath = [[utility getDocumentSubPath:@"Music"] stringByAppendingPathComponent:[postInfoComponents lastObject]];
                        //filePath = [[utility getDocumentSubPath:@"tmp"] stringByAppendingPathComponent:[postInfoComponents lastObject]];
						notificationPostName = @"Music";
                        NSNumber* num = (NSNumber*)[uploadedMap objectForKey:[NSNumber numberWithInt:2]];
                        if (num == nil) {
                            num = [NSNumber numberWithInt:1];
                        }
                        else
                        {
                            num = [NSNumber numberWithInt:[num intValue]+1];
                        }
                        [uploadedMap setObject:num forKey:[NSNumber numberWithInt:2]];
					}
					else if (UTTypeConformsTo(fileUTI, kUTTypeMovie))
					{
                        isImage = false;
						filePath = [[utility getDocumentSubPath:@"Video"] stringByAppendingPathComponent:[postInfoComponents lastObject]];
                        //filePath = [[utility getDocumentSubPath:@"tmp"] stringByAppendingPathComponent:[postInfoComponents lastObject]];
						notificationPostName = @"Video";
                        NSNumber* num = (NSNumber*)[uploadedMap objectForKey:[NSNumber numberWithInt:1]];
                        if (num == nil) {
                            num = [NSNumber numberWithInt:1];
                        }
                        else
                        {
                            num = [NSNumber numberWithInt:[num intValue]+1];
                        }
                        [uploadedMap setObject:num forKey:[NSNumber numberWithInt:1]];
					}
					else {
                        isImage = false;
						filePath = [[utility getDocumentSubPath:@"Other"] stringByAppendingPathComponent:[postInfoComponents lastObject]];
                        //filePath = [[utility getDocumentSubPath:@"tmp"] stringByAppendingPathComponent:[postInfoComponents lastObject]];
						notificationPostName = @"Other";
                        NSNumber* num = (NSNumber*)[uploadedMap objectForKey:[NSNumber numberWithInt:3]];
                        if (num == nil) {
                            num = [NSNumber numberWithInt:1];
                        }
                        else
                        {
                            num = [NSNumber numberWithInt:[num intValue]+1];
                        }
                        [uploadedMap setObject:num forKey:[NSNumber numberWithInt:3]];
					}
					
                    NSLog(@"save file:%@",filePath);
                    @synchronized(lockForUploadCount)
                    {
                        uploadCount++;//used by photoviewcontroller
                        NSLog(@"uploadcount plus %d", uploadCount);
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"4photoupload" object:nil];//used by photoviewcontroller

					NSRange fileDataRange = {dataStartIndex, [postDataChunk length] - dataStartIndex};
					[[NSFileManager defaultManager] createFileAtPath:filePath contents:[postDataChunk subdataWithRange:fileDataRange] attributes:nil];
					NSFileHandle *file = [[NSFileHandle fileHandleForUpdatingAtPath:filePath] retain];
					if (file)
					{
						[file seekToEndOfFile];
                        NSLog(@"add");
						[multipartData addObject:file];
					}
					

					[postInfo release];

					break;
				}
			}
		}
	}
	else //表单值域已经处理过了 这之后的数据全是文件数据流
	{
		NSLog(@"post ok write data");
        if ([[multipartData lastObject] respondsToSelector:@selector(writeData:)])
        {
            [(NSFileHandle*)[multipartData lastObject] writeData:postDataChunk];
        }
	}

    
    double uploadProgress = (double)requestContentLengthReceived / requestContentLength;
//    NSNumber* progressValue = [NSNumber numberWithDouble:uploadProgress];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"4photouploadprogress" object:progressValue];//used by photoviewcontroller

//	NSLog(@"post ok write data end progress%f", uploadProgress);
	if (uploadProgress == 1.0) {
		NSLog(@"post ok write data ok staticLastObj:%@",staticLastObj);
		if (isImage && staticLastObj != nil)
		{
			NSString* filename = [[utility getDocumentSubPath:@"MyUpload"] stringByAppendingPathComponent:staticLastObj];
			UIImage* img = [[UIImage alloc] initWithContentsOfFile: filename];
            
            NSLog(@"file:%@ photo%@", filename, img);
//            [staticLastObj retain];
            NSLog(@"retain count 2: %lu", (unsigned long)[staticLastObj retainCount]);
            @synchronized(lockForUploadCount)
            {
                uploadCount--;//used by photoviewcontroller
                NSLog(@"uploadcount minus %d", uploadCount);
            }
            [DirManager savePhoto:img];//in this function, we will send the notification to photoviewcontroller
//            [lastObj release];
            
//            lastObj = nil;
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationPostName object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"4tab" object:nil];
        }
	}
}

- (BOOL)shouldDie
{
	postHeaderOK = FALSE;
    NSLog(@"should die");
	multipartData = nil;
    dataStartIndex = 0;

    [staticLastObj release];
    staticLastObj = nil;

	return [super shouldDie];
}


//检查是否已经处理完了multipart/form-data表单中的表单变量
- (BOOL) isBeginOfOctetStream
{
    NSString *octetStreamFlag = @"Content-Type: application/octet-stream";
    NSString *findData = [[NSString alloc] initWithData:(NSData *)[multipartData lastObject] encoding:NSUTF8StringEncoding];
    
    for (NSData *d in multipartData) {
        NSString *temp = [[[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding] autorelease] ;
        NSLog(@"multipartData items: %@", temp);
    }
        //如果已经处理完了multipart/form-data表单中的表单变量
    if ( findData != nil && [findData length] > 0 ) 
    {
        NSLog(@"findData is :%@\n octetStreamFlag is :%@", findData, octetStreamFlag);
        if ([octetStreamFlag isEqualToString:findData]) {
            NSLog(@"multipart/form-data 变量值域数据处理完毕");
            [findData release];
            return YES;
        }
        [findData release];
        return NO;
    }
    return NO;
    
}


@end
