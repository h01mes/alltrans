//
//  DirManager.h
//  FlyTransfer
//
//  Created by apple on 13-8-28.
//  Copyright (c) 2013年 Calibri. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSOperationQueue *g_saveQueue;
extern int uploadint;


@interface DirManager : NSObject


+ (NSMutableDictionary*)getDirMap;

+ (void)initDirMap;

+ (NSMutableDictionary*)getUploadedMap;

+ (int)loadResources;

+ (void)savePhoto:(UIImage *)photo;

+ (bool)isSync;


@end
